package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.models.Resume;
import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.bitbucket.ausholokhov.orientois.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Anton on 20.12.2015.
 */
@Controller
public class ResumeController {

    @Autowired
    private ResumeService resumeService;
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/resumeSave", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity resumeSave(@ModelAttribute Resume resume, HttpServletRequest request) {
        try {
            Employee sessionEmployee = (Employee) request.getSession().getAttribute("sessionEmployee");
            if (sessionEmployee.getResume().getId().equals(resume.getId()) || sessionEmployee.getAccount().getAdmin())
                resumeService.update(resume);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/downloadPDF", method = RequestMethod.POST)
    public ModelAndView downloadPDF(@RequestParam("id") Long id, HttpServletRequest request) {
        Employee employee = employeeService.get(id);
        request.setAttribute("employee", employee);
        return new ModelAndView("pdfView");
    }
}
