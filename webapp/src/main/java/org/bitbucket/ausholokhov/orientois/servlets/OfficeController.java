package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.Department;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.models.Office;
import org.bitbucket.ausholokhov.orientois.models.dto.OfficeDto;
import org.bitbucket.ausholokhov.orientois.service.DepartmentService;
import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.bitbucket.ausholokhov.orientois.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Anton on 28.01.2016.
 */
@Controller
public class OfficeController {

    @Autowired
    private OfficeService officeService;
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/searchOfficeAutocomplete")
    @ResponseBody
    public List<OfficeDto> searchAutocomplete(@RequestParam("criteria") String criteria) {
        return officeService.searchOfficeDtoByCriteria(criteria);
    }

    @RequestMapping(value = "/officeEdit", method = RequestMethod.GET)
    public ModelAndView officeEdit(@RequestParam("id") long id, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("officeEdit");
        Employee sessionEmployee = (Employee) request.getSession().getAttribute("sessionEmployee");
        if (sessionEmployee.getAccount().getAdmin()) {
            Office office = officeService.get(id);
            modelAndView.addObject("office", office);
            List<Department> departments = departmentService.getAll();
            modelAndView.addObject("departments", departments);
        } else {
            modelAndView.setViewName("accessDeniedPage");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/officeSave", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity officeSave(@RequestParam("id") Long id,
                                     @RequestParam("name") String name,
                                     @RequestParam("department.id") Long depId,
                                     @RequestParam("chiefId") Long chiefId) {
        try {
            Office office = officeService.get(id);
            office.setName(name);
            office.setDepartment(departmentService.get(depId));
            Employee chief = employeeService.get(chiefId);
            office.setChief(chief);
            officeService.update(office);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/officeDelete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity officeDelete(@RequestParam("id") Long id) {
        try {
            officeService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
