package org.bitbucket.ausholokhov.orientois.servlets.pdf;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.models.Resume;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Map;

/**
 * Created by Anton on 16.01.2016.
 */
public class PDFBuilder extends AbstractPdfView {

    public static final String BIRTH_DATE = "Дата рождения: ";
    public static final String EMAIL = "E-mail: ";
    public static final String MAIN_EDU = "Основное образование: ";
    public static final String PRIOR_WORKPLACE = "Предыдущее место работы: ";
    public static final String ADD_EDU = "Дополнительное образование: ";
    public static final String ADD_SKILLS = "Дополнительные навыки: ";
    public static final String PROF_EXP = "Профессиональный опыт: ";
    public static final String CONTACT_INFO = "Контактная информация: ";
    public static final String ADD_INFO = "Дополнительная информация: ";
    public static final String HAS_CHILDREN = "Есть дети: ";
    public static final String READY_FOR_RELOCATION = "Готовность к переезду: ";
    private static final char CHECKED = '\u00FE';
    private static final char UNCHECKED = '\u00A8';

    @Override
    protected void buildPdfDocument(Map<String, Object> model, Document document, PdfWriter writer,
                                    HttpServletRequest request, HttpServletResponse response) throws Exception {

        Employee employee = (Employee) request.getAttribute("employee");
        Resume resume = employee.getResume();
        Image image = Image.getInstance(employee.getPersonalCard().getPhotoBytes());
        image.setAbsolutePosition(400f, 670f);
        image.scaleAbsolute(140f, 140f);
        image.setUseVariableBorders(true);
        image.setBorder(Rectangle.BOX);
        image.setBorderWidth(10);
        image.setBorderColor(BaseColor.GRAY);
        document.add(image);

        Font checkBoxes = new Font(BaseFont.createFont("/wingding.ttf", BaseFont.IDENTITY_H, false), 16f, Font.BOLD);
        Font tahomaBold = new Font(BaseFont.createFont("/tahomabd.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));
        Font resumeFont = new Font(BaseFont.createFont("/resumeFont.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED));

        resumeFont.setSize(24f);

        String name = employee.getLastname() + " " + employee.getFirstname() + " " + employee.getMiddlename();
        Paragraph fullName = new Paragraph(name, resumeFont);
        fullName.setSpacingAfter(15f);
        document.add(fullName);

        resumeFont.setSize(14f);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Paragraph parDate = new Paragraph(20f);
        parDate.add(new Chunk(BIRTH_DATE, tahomaBold));
        parDate.add(new Chunk(dateFormat.format(resume.getBirthdate()), resumeFont));
        document.add(parDate);

        Paragraph parContact = new Paragraph(20f);
        parContact.add(new Chunk(CONTACT_INFO, tahomaBold));
        parContact.add(new Chunk(resume.getContactInfo(), resumeFont));
        document.add(parContact);

        Paragraph parEmail = new Paragraph(20f);
        parEmail.add(new Chunk(EMAIL, tahomaBold));
        parEmail.add(new Chunk(employee.getEmail(), resumeFont));
        parEmail.setSpacingAfter(35F);
        document.add(parEmail);

        Paragraph mainEdu = new Paragraph();
        mainEdu.setSpacingBefore(10f);
        mainEdu.add(new Paragraph(MAIN_EDU, tahomaBold));
        mainEdu.add(new Paragraph(resume.getMainEducation(), resumeFont));
        document.add(mainEdu);

        Paragraph addEdu = new Paragraph();
        addEdu.setSpacingBefore(10f);
        addEdu.add(new Paragraph(ADD_EDU, tahomaBold));
        addEdu.add(new Paragraph(resume.getAdditionalEducation(), resumeFont));
        document.add(addEdu);

        Paragraph priorWorkplace = new Paragraph();
        priorWorkplace.setSpacingBefore(10f);
        priorWorkplace.add(new Paragraph(PRIOR_WORKPLACE, tahomaBold));
        priorWorkplace.add(new Paragraph(resume.getPriorWorkplace(), resumeFont));
        document.add(priorWorkplace);

        Paragraph profExp = new Paragraph();
        profExp.setSpacingBefore(10f);
        profExp.add(new Paragraph(PROF_EXP, tahomaBold));
        profExp.add(new Paragraph(resume.getProfessionalExpirience(), resumeFont));
        document.add(profExp);

        Paragraph addSkills = new Paragraph();
        addSkills.setSpacingBefore(10f);
        addSkills.add(new Paragraph(ADD_SKILLS, tahomaBold));
        addSkills.add(new Paragraph(resume.getSkills(), resumeFont));
        document.add(addSkills);

        Paragraph hasChildren = new Paragraph();
        hasChildren.setSpacingBefore(10f);
        hasChildren.add(new Paragraph(HAS_CHILDREN, tahomaBold));
        hasChildren.add(new Paragraph(String.valueOf(resume.isHasChildren() ? CHECKED : UNCHECKED), checkBoxes));
        document.add(hasChildren);

        Paragraph readyForRelocation = new Paragraph();
        readyForRelocation.setSpacingBefore(10f);
        readyForRelocation.add(new Paragraph(READY_FOR_RELOCATION, tahomaBold));
        readyForRelocation.add(new Paragraph(String.valueOf(resume.isReadyForRelocation() ? CHECKED : UNCHECKED), checkBoxes));
        document.add(readyForRelocation);

        Paragraph addInfo = new Paragraph();
        addInfo.setSpacingBefore(10f);
        addInfo.add(new Paragraph(ADD_INFO, tahomaBold));
        addInfo.add(new Paragraph(resume.getAdditionalData(), resumeFont));
        document.add(addInfo);

        document.close();
        writer.close();
    }
}
