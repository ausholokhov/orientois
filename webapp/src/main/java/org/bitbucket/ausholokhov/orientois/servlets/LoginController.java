package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.service.AccountService;
import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.bitbucket.ausholokhov.orientois.service.utils.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Anton on 30.11.2015.
 */
@Controller
public class LoginController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private EmployeeService employeeService;


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView login(@RequestParam("login") String enteredLogin,
                              @RequestParam("password") String enteredPassword
            , HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView("login");
        Account account;
        try {
            account = accountService.getByLogin(enteredLogin);
        } catch (NoResultException e) {
            modelAndView.addObject("notValidAccount", true);
            return modelAndView;
        }
        if (EncryptUtils.checkPassword(enteredPassword, account.getPassword())) {
            modelAndView.addObject("account", account);
            Employee employee = employeeService.getEmployeeByAccount(account);
            request.getSession().setAttribute("sessionEmployee", employee);
            if (request.getParameter("remember-me") != null) {
                String cookieValue = EncryptUtils.encryptText(String.valueOf(employee.getId()));
                Cookie loginCookie = new Cookie("employeeId", cookieValue);
                loginCookie.setMaxAge(99999999);
                response.addCookie(loginCookie);
            }
        } else {
            modelAndView.addObject("notValidAccount", true);
        }
        return modelAndView;
    }

    @RequestMapping(value = "/exit", method = RequestMethod.GET)
    private void exit(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().setAttribute("sessionEmployee", null);
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            Cookie cookie = new Cookie("employeeId", null);
            cookie.setMaxAge(0);
            resp.addCookie(cookie);
        }
        req.getRequestDispatcher("/login").forward(req, resp);
    }
}
