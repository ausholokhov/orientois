package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.*;
import org.bitbucket.ausholokhov.orientois.service.*;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by Anton on 27.01.2016.
 */
@Controller
public class AdminController {

    @Autowired
    private NewsService newsService;


    @RequestMapping(value = "/adminPage", method = RequestMethod.GET)
    public ModelAndView adminPage(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("adminPage");
        Employee employee = (Employee) request.getSession().getAttribute("sessionEmployee");
        if (!employee.getAccount().getAdmin()) {
            modelAndView.setViewName("accessDeniedPage");
        }
        modelAndView.addObject("news", newsService.getAll());
        return modelAndView;
    }

    @RequestMapping(value = "/addNews", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addNews(@RequestParam("image") MultipartFile img,
                                  @RequestParam("body") String body,
                                  @RequestParam("header") String header) {
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(img.getBytes());
            BufferedImage scaledImage = Scalr.resize(ImageIO.read(bais), 1250, 500);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(scaledImage, "png", baos);
            News news = new News();
            news.setHeader(header);
            news.setBody(body);
            news.setImage(baos.toByteArray());
            newsService.add(news);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/deleteNews", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity deleteNews(@RequestParam("deleteNewsId") Long id) {
        try {
            newsService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
