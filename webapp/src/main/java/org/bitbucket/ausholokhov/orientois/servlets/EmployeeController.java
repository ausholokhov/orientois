package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.*;
import org.bitbucket.ausholokhov.orientois.models.dto.EmployeeDto;
import org.bitbucket.ausholokhov.orientois.service.*;
import org.bitbucket.ausholokhov.orientois.service.utils.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anton on 02.12.2015.
 */
@Controller
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private OfficeService officeService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private ResumeService resumeService;
    @Autowired
    private PersonalCardService personalCardPersonalCardService;

    @RequestMapping(value = "/searchEmployee", method = RequestMethod.GET)
    public ModelAndView search(@RequestParam(value = "criteria", required = false) String criteria) {
        ModelAndView modelAndView = new ModelAndView("extendedSearch");
        if (criteria != null && !criteria.isEmpty()) {
            List<Employee> employees = employeeService.criteriaFilter(employeeService.getAll(), criteria);
            modelAndView.addObject("employees", employees);
        }
        modelAndView.addObject("offices", officeService.getAll());
        return modelAndView;
    }

    @RequestMapping(value = "/searchExtended", method = RequestMethod.POST)
    public ModelAndView searchWithFilter(@RequestParam(value = "extendedCriteria", required = false) String exCriteria,
                                         @RequestParam(value = "office.id", required = false) String officeId) {
        ModelAndView modelAndView = new ModelAndView("extendedSearch");
        List<Employee> employees = null;
        if (officeId != null && !officeId.isEmpty()) {
            employees = employeeService.getByOffice(Long.parseLong(officeId));
            if (exCriteria != null) {
                employees = employeeService.criteriaFilter(employees, exCriteria);
            }
        } else if (exCriteria != null && !exCriteria.isEmpty()) {
            employees = employeeService.getEmployeesByCriteria(exCriteria);
        }
        modelAndView.addObject("offices", officeService.getAll());
        modelAndView.addObject("employees", employees);
        return modelAndView;
    }

    @RequestMapping(value = "/searchEmployeeAutocomplete")
    @ResponseBody
    public List<EmployeeDto> searchAutocomplete(@RequestParam("criteria") String criteria) {
        List<EmployeeDto> employeesDto = new ArrayList<>();
        if (criteria != null && !criteria.isEmpty()) {
            List<Employee> employees = employeeService.getEmployeesByCriteria(criteria);
            for (Employee employee : employees) {
                EmployeeDto employeeDto = new EmployeeDto();
                employeeDto.id = employee.getId();
                employeeDto.firstname = employee.getFirstname();
                employeeDto.middlename = employee.getMiddlename();
                employeeDto.lastname = employee.getLastname();
                employeeDto.email = employee.getEmail();
                employeesDto.add(employeeDto);
            }
        }
        return employeesDto;
    }

    @RequestMapping(value = "/employeeInfo", method = RequestMethod.GET)
    public ModelAndView employeeInfo(@RequestParam("id") long id) {
        ModelAndView modelAndView = new ModelAndView("employeeInfo");
        Employee employee = employeeService.get(id);
        modelAndView.addObject("employee", employee);
        return modelAndView;
    }

    @RequestMapping(value = "/employeeEdit", method = RequestMethod.GET)
    public ModelAndView employeeEdit(@RequestParam("id") long id, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("employeeEdit");
        Employee sessionEmployee = (Employee) request.getSession().getAttribute("sessionEmployee");
        if (sessionEmployee.getId() == id || sessionEmployee.getAccount().getAdmin()) {
            Employee employee = employeeService.get(id);
            modelAndView.addObject("employee", employee);
            List<Office> offices = officeService.getAll();
            offices.remove(employee.getOffice());
            modelAndView.addObject("offices", offices);
        } else {
            modelAndView.setViewName("accessDeniedPage");
        }
        return modelAndView;
    }

    @RequestMapping(value = "/employeeSave", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity employeeSave(@ModelAttribute Employee employee) {
        try {
            Account account = accountService.get(employee.getAccount().getId());
            Resume resume = resumeService.get(employee.getResume().getId());
            PersonalCard card = personalCardPersonalCardService.get(employee.getPersonalCard().getId());
            employee.setAccount(account);
            employee.setResume(resume);
            employee.setPersonalCard(card);
            employeeService.update(employee);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/employeeDelete", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity employeeDelete(@RequestParam("id") Long id) {
        try {
            employeeService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/employeeCreate", method = RequestMethod.POST)
    public String employeeCreate(@ModelAttribute Employee employee, @ModelAttribute Account account) {
        account.setPassword(EncryptUtils.encryptPassword(account.getPassword()));
        employee.setAccount(account);
        employee.setResume(new Resume());
        employee.setPersonalCard(new PersonalCard());
        employeeService.add(employee);
        return "redirect:/login";
    }
}
