package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.models.PersonalCard;
import org.bitbucket.ausholokhov.orientois.models.Sticker;
import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.bitbucket.ausholokhov.orientois.service.PersonalCardService;
import org.bitbucket.ausholokhov.orientois.service.StickerService;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by Anton on 28.11.2015.
 */
@Controller
public class PersonalCardController {

    @Autowired
    private PersonalCardService cardService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private StickerService stickerService;

    @RequestMapping(value = "/personalCard", method = RequestMethod.GET)
    public ModelAndView showCard(@RequestParam("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("personalCard");
        Employee employee = employeeService.get(id);
        modelAndView.addObject("employee", employee);
        return modelAndView;
    }

    @RequestMapping(value = "/doUpload", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity handleImgUpload(@RequestParam("file") MultipartFile img,
                                          @RequestParam("id") Long id,
                                          HttpServletRequest request) {
        try {
            if (checkCardAccess(id, request)) {
                PersonalCard card = cardService.get(id);
                ByteArrayInputStream bais = new ByteArrayInputStream(img.getBytes());
                BufferedImage scaledImage = Scalr.resize(ImageIO.read(bais), 300);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(scaledImage, "png", baos);
                card.setPhoto(baos.toByteArray());
                cardService.update(card);
            } else {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/savePost", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity savePost(@ModelAttribute PersonalCard newCard, HttpServletRequest request) {
        try {
            if (checkCardAccess(newCard.getId(), request)) {
                PersonalCard oldCard = cardService.get(newCard.getId());
                oldCard.setPost(newCard.getPost());
                cardService.update(oldCard);
            } else {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping("/saveSticker")
    @ResponseBody
    public Long saveSticker(@ModelAttribute Sticker sticker,
                            @RequestParam("cardId") Long cardId, @RequestParam("userId") Long userId) {
        Employee user = employeeService.get(userId);
        if (user.getAccount().getAdmin() || user.getPersonalCard().getId().equals(sticker.getPersonalCard().getId())) {
            sticker.setPersonalCard(cardService.get(cardId));
            sticker = stickerService.add(sticker);
        }
        return sticker.getId();
    }

    @RequestMapping("/deleteSticker")
    @ResponseBody
    public ResponseEntity deleteSticker(@RequestParam("id") Long id, @RequestParam("userId") Long userId) {
        Sticker sticker = stickerService.get(id);
        Employee user = employeeService.get(userId);
        if (user.getAccount().getAdmin() || user.getPersonalCard().getId().equals(sticker.getPersonalCard().getId())) {
            stickerService.delete(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private boolean checkCardAccess(Long cardId, HttpServletRequest request) {
        Employee sessionEmployee = (Employee) request.getSession().getAttribute("sessionEmployee");
        return sessionEmployee.getPersonalCard().getId().equals(cardId) || sessionEmployee.getAccount().getAdmin();
    }
}
