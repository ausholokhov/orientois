package org.bitbucket.ausholokhov.orientois.servlets.interceptors;

import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.bitbucket.ausholokhov.orientois.service.utils.EncryptUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Created by Anton on 10.12.2015.
 */
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private EmployeeService employeeService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (request.getSession().getAttribute("sessionEmployee") != null) {
            return true;
        }
        String uri = request.getRequestURI();
        if (uri.endsWith("/login.htm") || uri.endsWith("/employeeCreate")) {
            return true;
        }
        Cookie[] cookie = request.getCookies();
        for (Cookie cooky : cookie) {
            if (cooky.getName().equals("employeeId")) {
                String decryptedId = EncryptUtils.decryptText(cooky.getValue());
                if (decryptedId != null) {
                    Long id = Long.valueOf(decryptedId);
                    HttpSession session = request.getSession();
                    session.setAttribute("sessionEmployee", employeeService.get(id));
                    return true;
                }
            }
        }
        request.getRequestDispatcher("/login").forward(request, response);
        return false;
    }
}
