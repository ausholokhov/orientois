package org.bitbucket.ausholokhov.orientois.servlets;

import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.models.Sticker;
import org.bitbucket.ausholokhov.orientois.service.NewsService;
import org.bitbucket.ausholokhov.orientois.service.PersonalCardService;
import org.bitbucket.ausholokhov.orientois.service.StickerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class OrgController {

    @Autowired
    private NewsService newsService;

    @RequestMapping("/showHierarchy")
    public ModelAndView showHierarchy(HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView("hierarchy");
        Employee sessionEmployee = (Employee) request.getSession().getAttribute("sessionEmployee");
        if (sessionEmployee.getOffice() != null) {
            Employee chief = sessionEmployee.getOffice().getChief();
            List<Employee> collegues = sessionEmployee.getOffice().getEmployees();
            collegues.remove(chief);
            modelAndView.addObject("collegues", collegues);
            modelAndView.addObject("chief", chief);
        }
        return modelAndView;
    }

    @RequestMapping("/mainPage")
    public ModelAndView getNews() {
        ModelAndView modelAndView = new ModelAndView("mainPage");
        modelAndView.addObject("news", newsService.getAll());
        return modelAndView;
    }
}