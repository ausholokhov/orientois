<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--Страница админа--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a data-toggle="pill" href="#newsTab">Новости</a></li>
                <li><a data-toggle="pill" href="#editTab">Редактирование</a></li>
            </ul>
        </div>
        <div class="col-sm-8 text-left">
            <div class="container">
                <div class="tab-content">
                    <div id="newsTab" class="tab-pane fade in active">
                        <form id="news" method="POST"
                              action="<c:url value="/addNews" />"
                              enctype="multipart/form-data">
                            <input type="hidden" name="id" id="id" value="">
                            <h3 align="center">Новости</h3>
                            <div class="container">
                                <div class="row">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <h4>Иллюстрация к новости</h4>
                                                <div class="input-group image-preview">
                                                    <input type="text" class="form-control image-preview-filename"
                                                           disabled="disabled" placeholder="Рекомендуется 1920x1080">

                <span class="input-group-btn">
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg" name="image" id="image"/>
                    </div>
                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h4>Заголовок новости</h4>
                                <input class="form-control" id="header" type="text"
                                       value="" title="" name="header">

                                <h4>Тело новости </h4>
                                            <textarea class="form-control vresize" rows="20"
                                                      name="body"
                                                      id="body"
                                                      title=""></textarea>
                                <br>
                                <button type="submit" class="btn btn-success" id="addNews">
                                    <i class="glyphicon glyphicon-ok"></i> Добавить новость
                                </button>
                            </div>
                        </form>
                        <div class="container">
                            <h4>Удаление новости </h4>
                            <form action="<c:url value="/deleteNews"/>" method="post" id="deleteNewsForm">
                                    <select class="form-control" title="" name="deleteNewsId">
                                        <c:forEach items="${news}" var="news">
                                            <option value="${news.id}">${news.header}</option>
                                        </c:forEach>
                                    </select>
                                <br>
                                <button type="submit" class="btn btn-danger" id="deleteNewsButton">
                                    <i class="glyphicon glyphicon-trash"></i> Удалить новость
                                </button>
                            </form>
                        </div>
                    </div>
                    <div id="editTab" class="tab-pane fade">
                        <h3 align="center">Редактирование</h3>
                        <div class="container">
                            <div class="row">
                                <h4>Найти и редактировать сотрудника</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="searchEmployeeInput"
                                           placeholder="Введите критерий поиска...">
                                </div>

                                <div id="employeeSearchResult">
                                </div>

                                <br>
                                <h4>Найти и редактировать отдел</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="searchOfficeInput"
                                           placeholder="Введите критерий поиска...">
                                </div>

                                <div id="officeSearchResult">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="footer.jsp" %>

<script src=<c:url value="/resources/js/jquery-ui.js"/>></script>
<link rel="stylesheet" href=<c:url value="/resources/css/jquery-ui.css"/>>
<script>

    $(function () {
        $("#searchEmployeeInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<c:url value="/searchEmployeeAutocomplete"/>',
                    data: {
                        criteria: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (employee) {
                            $("#employeeSearchResult").html("");
                            $("#employeeSearchResult").html('<a href="<c:url value="/employeeEdit?id="/>' + employee.id + '">' + employee.lastname + " " + employee.firstname + " " + employee.middlename + '</a>');
                        }));
                    }
                });
            },
            minLength: 3
        });
    });

    $(function () {
        $("#searchOfficeInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<c:url value="/searchOfficeAutocomplete"/>',
                    data: {
                        criteria: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (office) {
                            $("#officeSearchResult").html("");
                            $("#officeSearchResult").html('<a href="<c:url value="/officeEdit?id="/>' + office.id + '">' + office.name + '</a>');
                        }));
                    }
                });
            },
            minLength: 3
        });
    });


    $('#deleteNewsForm').submit(function (evt) {
        evt.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function () {
                swal('Новость удалена', '', 'success');
            },
            error: function () {
                swal('Удаление не удалось', '', 'error');
            }
        });
    });


    $('#news').submit(function (evt) {
        evt.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function () {
                swal('Новость добавлена', '', 'success');
            },
            error: function () {
                swal('Изменения не сохранены', 'Попробуйте заменить картинку', 'error');
            }
        });
    });

    $(document).on('click', '#close-preview', function () {
        $('.image-preview').popover('hide');
        $('.image-preview').hover(
                function () {
                    $('.image-preview').popover('show');
                },
                function () {
                    $('.image-preview').popover('hide');
                }
        );
    });

    $(function () {
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;'
        });
        closebtn.attr("class", "close pull-right");
        $('.image-preview').popover({
            trigger: 'manual',
            html: true,
            title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
            content: "There's no image",
            placement: 'bottom'
        });
        $('.image-preview-clear').click(function () {
            $('.image-preview').attr("data-content", "").popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse");
        });
        $(".image-preview-input input:file").change(function () {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 200
            });
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-filename").val(file.name);
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
            };
            reader.readAsDataURL(file);
        });
    });
</script>

<style>
    #searchclear {
        position: absolute;
        right: 5px;
        top: 0;
        bottom: 0;
        height: 14px;
        margin: auto;
        font-size: 14px;
        cursor: pointer;
        color: #ccc;
    }

    @media screen and (min-width: 768px) {

    }

    .container {
        margin-top: 10px;
    }

    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }

    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .image-preview-input-title {
        margin-left: 2px;
    }
</style>
