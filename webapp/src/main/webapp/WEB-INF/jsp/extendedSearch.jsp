<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--Расширенный поиск--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>
<div class="container">
    <div class="row">
        <h2>Поиск сотрудников:</h2>
        <div class="col-md-12">
            <div class="input-group" id="adv-search">
                <input type="text" id="searchInput" class="form-control" placeholder="Введите критерий поиска..."/>
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu" role="menu">
                                <form method="post" id="adv-search-form" class="form-horizontal" role="form"
                                      action="<c:url value="/searchExtended"/>">
                                    <label>Отдел</label>
                                    <div class="form-group">
                                        <select class="form-control" title="" name="office.id">
                                            <option value="" selected>По всем</option>
                                            <c:forEach items="${offices}" var="office">
                                                <option value="${office.id}">${office.name}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <label>Часть фамилии/имени/отчества</label>
                                    <div class="form-group">
                                        <input class="form-control" type="text" name="extendedCriteria"/>
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary" form="adv-search-form" onclick="startSearch()">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="info.jsp"/>
<%@include file="footer.jsp" %>


<script src=<c:url value="/resources/js/jquery-ui.js" />></script>
<link rel="stylesheet" href=<c:url value="/resources/css/jquery-ui.css" />>
<script>
    function startSearch() {
        var criteria = document.getElementById("searchInput").value;
        location.href = "${pageContext.request.contextPath}/searchEmployee.htm?criteria=" + criteria;
    }

    $(function () {
        function log(message) {
            $("<div>").text(message).prependTo("#log");
            $("#log").scrollTop(0);
        }

        $("#searchInput").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<c:url value="/searchEmployeeAutocomplete"/>',
                    data: {
                        criteria: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (employee) {
                            return {
                                label: employee.firstname + " " + employee.middlename + " " + employee.lastname + " " + employee.email,
                                value: employee.firstname + " " + employee.middlename + " " + employee.lastname + " " + employee.email
                            }
                        }));
                    }
                });
            },
            minLength: 2
        });
    });
</script>

<style>
    .dropdown.dropdown-lg .dropdown-menu {
        margin-top: -1px;
        padding: 6px 20px;
        margin-left: -430px;
    }

    .input-group-btn .btn-group {
        display: flex !important;
    }

    .btn-group .btn {
        border-radius: 0;
        margin-left: -1px;
    }

    .btn-group .btn:last-child {
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }

    .btn-group .form-horizontal .btn[type="submit"] {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    .form-horizontal .form-group {
        margin-left: 0;
        margin-right: 0;
    }

    .form-group .form-control:last-child {
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    @media screen and (min-width: 768px) {
        #adv-search {
            width: 1200px;
            margin: 0 auto;
        }

        .dropdown.dropdown-lg {
            position: static !important;
        }

        .dropdown.dropdown-lg .dropdown-menu {
            min-width: 500px;
        }
    }
</style>

