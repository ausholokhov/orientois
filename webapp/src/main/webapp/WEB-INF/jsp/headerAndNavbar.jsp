<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html>
<head>
    <title>Orient OIS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel='stylesheet' href="<c:url value="/resources/css/sweetalert2.css"/>">
    <link rel='stylesheet' href="<c:url value="/resources/css/spectrum.css"/>">
    <link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap.min.css") %>'>
    <link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-theme.min.css") %>'>
    <link rel='stylesheet' href='<%= org.webjars.AssetLocator.getWebJarPath("css/bootstrap-datepicker3.css") %>'>
    <script type='text/javascript' src="<c:url value="/resources/js/sweetalert2.min.js"/>"></script>
    <script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.js") %>'></script>
    <script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("jquery.min.js") %>'></script>
    <script type='text/javascript' src="<c:url value="/resources/js/jquery-ui.js"/>"></script>
    <script type='text/javascript' src="<c:url value="/resources/js/spectrum.js"/>"></script>
    <script type='text/javascript' src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap.min.js") %>'></script>
    <script type='text/javascript'
            src='<%= org.webjars.AssetLocator.getWebJarPath("js/bootstrap-datepicker.js") %>'></script>
</head>
<body style="background-color: #f8f8f8;">
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src="<c:url value="/resources/label2.png" />" width="120px" height="25px"></a>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="<c:url value="/mainPage"/>">На домашнюю страницу</a></li>
                <li><a href="<c:url value="/searchEmployee"/>">Расширенный поиск</a></li>
                <li><a href="<c:url value="/employeeInfo?id=${sessionScope.get('sessionEmployee').id}"/>">Обзор</a></li>
                <li><a href="<c:url value="/showHierarchy"/>">Коллеги</a></li>
            </ul>

            <c:choose>
            <c:when test='${sessionScope.get("sessionEmployee")!=null}'>
                <ul class="nav navbar-nav navbar-right">
                    <c:if test="${sessionScope.get('sessionEmployee').account.admin}">
                        <li>
                            <a href="${pageContext.request.contextPath}/adminPage"><span
                                    class="glyphicon glyphicon-eye-open"></span> Администрирование</a>
                        </li>
                    </c:if>
                    <li>
                        <a href="<c:url value="/employeeInfo?id=${sessionScope.get('sessionEmployee').id}"/>"><span
                                class="glyphicon glyphicon-user"></span>
                                ${sessionScope.get("sessionEmployee").lastname}&nbsp
                                ${sessionScope.get("sessionEmployee").firstname}&nbsp
                                ${sessionScope.get("sessionEmployee").middlename}
                        </a>


                    </li>
                    <li><a href="${pageContext.request.contextPath}/exit"><span
                            class="glyphicon glyphicon-log-in"></span> Выйти</a></li>
                </ul>
            </c:when>
            <c:otherwise>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<c:url value="/login#register-form-link"/>"><span
                        class="glyphicon glyphicon-user"></span> Зарегистрироваться</a></li>
                <li><a href="<c:url value="/login"/>"><span
                        class="glyphicon glyphicon-log-in"></span> Войти</a></li>
                </c:otherwise>
                </c:choose>
            </ul>
            <c:if test="${sessionScope.get('sessionEmployee')!=null}">
                <form class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Введите критерий поиска...">
						<span class="input-group-btn">
							<button type="reset" class="btn btn-default">
								<span class="glyphicon glyphicon-remove">
									<span class="sr-only">Закрыть</span>
								</span>
                            </button>
							<button type="submit" class="btn btn-default" style="background: none"
                                    id="quickSearchButton">
								<span class="glyphicon glyphicon-search">
									<span class="sr-only">Search</span>
								</span>
                            </button>
						</span>
                    </div>
                </form>
            </c:if>
        </div>
    </div>
</nav>
<style>
    #quickSearchButton:hover span {
        color: #ffffff;
    }

    .navbar-collapse form[role="search"] {
        position: absolute;
        top: 0;
        right: 0;
        width: 100%;
        padding: 0;
        margin: 0;
        z-index: 0;
    }

    .navbar-collapse form[role="search"] button,
    .navbar-collapse form[role="search"] input {
        padding: 8px 12px;
        border-radius: 0;
        color: rgb(119, 119, 119);
        background-color: rgb(248, 248, 248);
        border: 0 rgb(231, 231, 231);
        box-shadow: none;
        outline: none;
    }

    .navbar-collapse form[role="search"] input {
        padding: 16px 12px;
        font-size: 14pt;
        font-style: italic;
        color: rgb(160, 160, 160);
        box-shadow: none;
    }

    .navbar-collapse form[role="search"] button[type="reset"] {
        display: none;
    }

    @media (min-width: 768px) {
        .navbar-collapse {
            padding-top: 0 !important;
            padding-right: 38px !important;
        }

        .navbar-collapse form[role="search"] {
            width: 38px;
        }

        .navbar-collapse form[role="search"] button,
        .navbar-collapse form[role="search"] input {
            padding: 15px 12px;
        }

        .navbar-collapse form[role="search"] input {
            font-size: 18pt;
            opacity: 0;
            display: none;
            height: 50px;
        }

        .navbar-collapse form[role="search"].active {
            width: 100%;
        }

        .navbar-collapse form[role="search"].active button,
        .navbar-collapse form[role="search"].active input {
            display: table-cell;
            opacity: 1;
        }

        .navbar-collapse form[role="search"].active input {
            width: 100%;
        }
    }

    .navbar-brand > img {
        max-width: 120px;
        margin-top: -4px
    }
</style>
<script>
    $(function () {
        $('body, .navbar-collapse form[role="search"] button[type="reset"]').on('click keyup', function (event) {
            console.log(event.currentTarget);
            if (event.which == 27 && $('.navbar-collapse form[role="search"]').hasClass('active') ||
                    $(event.currentTarget).attr('type') == 'reset') {
                var $form = $('.navbar-collapse form[role="search"].active');
                $form.find('input').val('');
                $form.removeClass('active');
            }
        });
        $(document).on('click', '.navbar-collapse form[role="search"]:not(.active) button[type="submit"]', function (event) {
            event.preventDefault();
            var $form = $(this).closest('form'),
                    $input = $form.find('input');
            $form.addClass('active');
            $input.focus();
        });
        $(document).on('click', '.navbar-collapse form[role="search"].active button[type="submit"]', function (event) {
            event.preventDefault();
            var $form = $(this).closest('form'),
                    $input = $form.find('input');
            var criteria = $input.val();
            if (criteria != "") {
                window.location.href = "searchEmployee?criteria=" + criteria;
            }
            $form.removeClass('active');
        });
    });
</script>