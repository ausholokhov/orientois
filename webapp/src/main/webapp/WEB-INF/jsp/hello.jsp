<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>

<%--первый сервлет выводит всех сотрудников --%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp"%>

<%@include file="info.jsp"%>

<%@include file="footer.jsp"%>