﻿﻿
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--форма для редактирования офиса--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container">
    <h1>Редактирование информации:</h1>

    <form:form name="officeForm" class="form-horizontal" role="form" method="post" id="officeForm">
        <input type="hidden" name="id" id="id" value="${office.id}">
        <c:choose>
            <c:when test="${office.chief!=null}">
                <input type="hidden" name="chiefId" id="chiefId" value="${office.chief.id}">
            </c:when>
            <c:otherwise>
                <input type="hidden" name="chiefId" id="chiefId" value="">
            </c:otherwise>
        </c:choose>
        <fieldset>
            <div class="form-group">

                <label class="col-sm-2 control-label">Название офиса:</label>
                <div class="col-sm-10">
                    <input class="form-control" id="name" type="text" name="name"
                           value="${office.name}" title="">
                </div>

                <label class="col-sm-2 control-label">Начальник:</label>
                <div class="col-sm-10">
                    <input class="form-control" id="chief" type="text"
                           value="${office.chief}" title="" name="chief">
                </div>

                <label class="col-sm-2 control-label">Департамент:</label>
                <div class="col-sm-10">
                    <select class="form-control" title="" name="department.id">
                        <option value="${office.department.id}">${office.department.name} (текущий)</option>
                        <c:forEach items="${departments}" var="department">
                            <option value="${department.id}">${department.name}</option>
                        </c:forEach>
                    </select>
                </div>

                <label class="col-sm-2 control-label"></label>

                <div class="col-sm-10">
                    <br>
                    <button id="saveOffice" type="button" class="btn btn-success" value="Сохранить">
                        <i class="glyphicon glyphicon-saved"></i> Сохранить
                    </button>
                    <button id="deleteOffice" type="button" class="btn btn-danger" data-toggle="modal">
                        <i class="glyphicon glyphicon-trash"></i> Удалить подразделение
                    </button>
                </div>
            </div>
        </fieldset>
    </form:form>
</div>

<script src=<c:url value="/resources/js/jquery-ui.js"/>></script>
<link rel="stylesheet" href=<c:url value="/resources/css/jquery-ui.css"/>>
<script>

    $("#chief").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: '<c:url value="/searchEmployeeAutocomplete"/>',
                data: {
                    criteria: request.term
                },
                success: function (data) {
                    response($.map(data, function (employee) {
                        $("#chiefId").attr("value", employee.id);
                        return {
                            label: employee.firstname + " " + employee.middlename + " " + employee.lastname + " " + employee.email,
                            value: employee.firstname + " " + employee.middlename + " " + employee.lastname + " " + employee.email
                        }
                    }));
                }
            });
        },
        minLength: 2
    });


    $("#saveOffice").click(function () {
        swal($.ajax({
            type: "POST",
            url: '<c:url value="/officeSave"/>',
            data: $("#officeForm").serialize(),
            dataType: 'text',
            timeout: 100000,
            success: function () {
                swal('Изменения сохранены', '', 'success');
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        }))
    });

    $("#deleteOffice").click(function () {
        swal({
            title: 'Вы уверены?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да, удалить!',
            cancelButtonText: 'Нет, не нужно!',
            confirmButtonClass: 'confirm-class',
            cancelButtonClass: 'cancel-class',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var officeId = $("#id").val();
                $.ajax({
                    type: "DELETE",
                    url: '<c:url value="/officeDelete?id="/>' + officeId,
                    dataType: 'text',
                    timeout: 100000,
                    success: function () {
                        swal('Удалено', '', 'success');
                    },
                    error: function () {
                        swal('Удаление отменено', 'Во время удаления произошла ошибка', 'error');
                    }
                })
            } else {
                swal('Удаление отменено', '', 'error');
            }
        });
    });
</script>
<style>
    .form-group {
        margin-bottom: 0;
    }

    .glyphicon {
        font-size: 12px;
    }
</style>

<%@include file="footer.jsp" %>