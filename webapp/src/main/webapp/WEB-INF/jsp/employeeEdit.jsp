﻿﻿
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--форма для редактирования сотрудника--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container">
    <h1>Редактирование информации:</h1>

    <form:form name="employeeForm" class="form-horizontal" role="form" method="post" id="employeeForm">
        <input type="hidden" name="id" id="id" value="${employee.id}">
        <input type="hidden" name="account.id" id="account.id" value="${employee.account.id}">
        <input type="hidden" name="resume.id" id="resume.id" value="${employee.resume.id}">
        <input type="hidden" name="personalCard.id" id="personalCard.id" value="${employee.personalCard.id}">
        <fieldset>
            <div class="form-group">
                <label class="col-sm-2 control-label">Фамилия:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="lastname" type="text" name="lastname"
                           value="${employee.lastname}" title="">
                </div>
                <label class="col-sm-2 control-label">Имя:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="firstname" type="text"
                           value="${employee.firstname}" title="" name="firstname">
                </div>
                <label class="col-sm-2 control-label">Отчество:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="middlename" type="text"
                           value="${employee.middlename}" title="" name="middlename">
                </div>
                <label class="col-sm-2 control-label">E-mail:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="email" type="text"
                           value="${employee.email}" title="" name="email">
                </div>
                <label class="col-sm-2 control-label">Адрес:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="address" type="text"
                           value="${employee.address}" title="" name="address">
                </div>
                <label class="col-sm-2 control-label">Отдел:</label>

                <div class="col-sm-10">
                    <select class="form-control" title="" name="office.id">
                        <option value="${employee.office.id}">${employee.office.name} (текущий)</option>
                        <c:forEach items="${offices}" var="office">
                            <option value="${office.id}">${office.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <label class="col-sm-2 control-label">Список контактов:</label>

                <div class="col-sm-10">
                    <div class="col-xs-3">
                        <div class="multiple-form-group" data-max=10>
                            <c:if test="${employee.contactPhones.size()==0}">
                                <div class="form-group input-group">
                                    <input type="text" name="contactPhones"
                                           class="form-control" value="" title="">
    					<span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-add">+
                            </button>
                        </span>
                                </div>
                            </c:if>
                            <c:forEach items="${employee.contactPhones}" var="phone" varStatus="status">
                                <div class="form-group input-group">
                                    <c:choose>
                                        <c:when test="${status.count<employee.contactPhones.size()}">
                                            <input type="text" name="contactPhones"
                                                   class="form-control" value="${phone}" title="">
    					<span class="input-group-btn">
                            <button type="button" class="btn btn-danger btn-remove">–</button>
                        </span>
                                        </c:when>
                                        <c:otherwise>
                                            <input type="text" name="contactPhones"
                                                   class="form-control" value="${phone}" title="">
    					<span class="input-group-btn">
                            <button type="button" class="btn btn-default btn-add">+</button>
                        </span>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                </div>
                <label class="col-sm-2 control-label"></label>

                <div class="col-sm-10">
                    <br>
                    <button id="saveEmployee" type="button" class="btn btn-success" value="Сохранить">
                        <i class="glyphicon glyphicon-saved"></i> Сохранить
                    </button>
                    <button id="deleteEmployee" type="button" class="btn btn-danger" data-toggle="modal">
                        <i class="glyphicon glyphicon-trash"></i> Удалить сотрудника
                    </button>
                </div>
            </div>
        </fieldset>
    </form:form>
</div>

<script>
    $("#saveEmployee").click(function () {
        swal($.ajax({
            type: "POST",
            url: '<c:url value="/employeeSave"/>',
            data: $("#employeeForm").serialize(),
            dataType: 'text',
            timeout: 100000,
            success: function () {
                swal('Изменения сохранены', '', 'success');
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        }))
    });

    $("#deleteEmployee").click(function () {
        swal({
            title: 'Вы уверены?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да, удалить!',
            cancelButtonText: 'Нет, не нужно!',
            confirmButtonClass: 'confirm-class',
            cancelButtonClass: 'cancel-class',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var employeeId = $("#id").val();
                $.ajax({
                    type: "DELETE",
                    url: '<c:url value="/employeeDelete?id="/>' + employeeId,
                    dataType: 'text',
                    timeout: 100000,
                    success: function () {
                        swal('Удалено', '', 'success');
                        setTimeout(function () {
                            window.location.replace('<c:url value="/mainPage"/>');
                        }, 1000);
                    },
                    error: function () {
                        swal('Удаление отменено', 'Во время удаления произошла ошибка', 'error');
                    }
                })
            } else {
                swal('Удаление отменено', '', 'error');
            }
        });
    });

    (function ($) {
        $(function () {
            var addFormGroup = function (event) {
                event.preventDefault();
                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();
                $(this).toggleClass('btn-default btn-add btn-danger btn-remove').html('–');
                $formGroupClone.find('input').val('');
                var g = $formGroupClone.find('input').val('');
                g.attr("name", "contactPhones");
                $formGroupClone.insertAfter($formGroup);
                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
            };
            var removeFormGroup = function (event) {
                event.preventDefault();
                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }
                $formGroup.remove();
            };
            var countFormGroup = function ($form) {
                return $form.find('.form-group').length;
            };
            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);
        });
    })(jQuery);
</script>
<style>
    .form-group {
        margin-bottom: 0;
    }

    .glyphicon {
        font-size: 12px;
    }
</style>

<%@include file="footer.jsp" %>