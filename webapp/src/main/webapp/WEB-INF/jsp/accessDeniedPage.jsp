<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--Появляется если сотрудник пытается редактировать не себя--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div align="center"><h2>У вас нет прав доступа к данной странице.</h2></div>

<%@include file="footer.jsp" %>