<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--подробная информация о сотруднике с выводом всех полей--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container">
    <h1>Подробная информация:</h1>

    <form class="form-horizontal" role="form" method="get">
        <fieldset disabled>
            <div class="form-group">
                <label class="col-sm-2 control-label">Фамилия:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="lastname" type="text"
                           value="${employee.lastname}" title="">
                </div>
                <label class="col-sm-2 control-label">Имя:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="firstname" type="text"
                           value="${employee.firstname}" title="">
                </div>
                <label class="col-sm-2 control-label">Отчество:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="middlename" type="text"
                           value="${employee.middlename}" title="">
                </div>
                <label class="col-sm-2 control-label">E-mail:</label>

                <div class="col-sm-10">
                    <div class="form-control">
                        <a href="mailto:${employee.email}">${employee.email}</a>
                    </div>
                </div>
                <label class="col-sm-2 control-label">Адрес:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="address" type="text"
                           value="${employee.address}" title="">
                </div>
                <label class="col-sm-2 control-label">Отдел:</label>

                <div class="col-sm-10">
                    <input class="form-control" id="office" type="text"
                           value="${employee.office.name}" title="">
                </div>
                <label class="col-sm-2 control-label">Список контактов:</label>

                <div class="col-sm-10">
                    <div id="flip">Показать/Скрыть</div>
                    <div id="panel">
                        <c:forEach items="${employee.contactPhones}" var="phone">
                            <div>
                                <input class="form-control" type="text"
                                       value="${phone}" title="">
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </fieldset>

        <div align="center">
            <button type="button" class="btn btn-success"
                    onclick='self.location.href = "<c:url value="/personalCard.htm?id=${employee.id}"/>"'>
                Посмотреть карточку
            </button>
            <c:set var="acc" value="${sessionScope.get('sessionEmployee').account}"/>
            <c:if test="${acc.id == employee.id || acc.admin}">
                <button type="button" class="btn btn-danger"
                        onclick='self.location.href = "<c:url value="/employeeEdit.htm?id=${employee.id}"/>"'>
                    Редактировать
                </button>
            </c:if>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        $("#flip").click(function () {
            $("#panel").slideToggle("slow");
        });
    });
</script>
<style>
    #panel, #flip {
        padding: 6px 12px;
        font-size: 14px;
        width: 100%;
        color: #555;
        background-color: #eeeeee;
        text-align: left;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    #panel {
        display: none;
    }
</style>

<%@include file="footer.jsp" %>