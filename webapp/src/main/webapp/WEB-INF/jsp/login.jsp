<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-login">
                <c:choose>
                    <c:when test="${sessionScope.get('sessionEmployee')!=null}">
                        <div class="alert alert-success" align="center">
                            <strong>Здравствуйте</strong>,
                                ${sessionScope.get("sessionEmployee").lastname}&nbsp
                                ${sessionScope.get("sessionEmployee").firstname}&nbsp
                                ${sessionScope.get("sessionEmployee").middlename}
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <input type="button"
                                           onclick="self.location.href='${pageContext.request.contextPath}/exit'"
                                           name="change-user"
                                           tabindex="4" class="form-control btn btn-change-user"
                                           value="Сменить пользователя">
                                </div>
                            </div>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-6">
                                    <a href="#" class="active" id="login-form-link">Войти</a>
                                </div>
                                <div class="col-xs-6">
                                    <a href="#" id="register-form-link">Регистрация</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form id="login-form" action="${pageContext.request.contextPath}/login.htm"
                                          method="post"
                                          role="form"
                                          style="display: block;">
                                        <c:if test="${notValidAccount!=null}">
                                            <div class="alert alert-danger">
                                                <strong>Аккаунта не найден!</strong> Проверьте введеные данные и
                                                попробуйте еще
                                                раз.
                                            </div>
                                        </c:if>
                                        <div class="form-group">
                                            <input type="text" name="login" id="login" tabindex="1" class="form-control"
                                                   placeholder="Логин" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" id="password" tabindex="2"
                                                   class="form-control" placeholder="Пароль" required>
                                        </div>
                                        <div class="form-group text-center">
                                            <input type="checkbox" tabindex="3" class="" name="remember-me"
                                                   id="remember-me">
                                            <label for="remember-me"> Запомнить меня</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" name="login-submit" id="login-submit"
                                                           tabindex="4"
                                                           class="form-control btn btn-login" value="Войти">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="text-center">
                                                        <a href="#" tabindex="5"
                                                           class="forgot-password">Забыли пароль?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <form id="register-form" action="${pageContext.request.contextPath}/employeeCreate"
                                          method="post"
                                          role="form" style="display: none;">
                                        <div class="form-group">
                                            <input type="text" name="lastname" id="lastname" tabindex="1"
                                                   class="form-control"
                                                   placeholder="Фамилия" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="firstname" id="firstname" tabindex="1"
                                                   class="form-control"
                                                   placeholder="Имя" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="middlename" id="middlename" tabindex="1"
                                                   class="form-control"
                                                   placeholder="Отчество" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" tabindex="1"
                                                   class="form-control"
                                                   placeholder="Email" value="" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="login" id="login" tabindex="1" class="form-control"
                                                   placeholder="Логин" value="">
                                        </div>

                                        <div class="form-group">
                                            <input type="password" class="input-lg form-control" tabindex="1"
                                                   name="password"
                                                   id="password1"
                                                   placeholder="Пароль" autocomplete="off" required>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-6">
                                        <span id="8char" class="glyphicon glyphicon-remove"
                                              style="color:#FF0004;"></span> Не менее 8 символов<br>
                                        <span id="ucase" class="glyphicon glyphicon-remove"
                                              style="color:#FF0004;"></span> Символы в верхнем регистре
                                            </div>
                                            <div class="col-sm-6">
                                        <span id="lcase" class="glyphicon glyphicon-remove"
                                              style="color:#FF0004;"></span> Символы в нижнем регистре<br>
                                                <span id="num" class="glyphicon glyphicon-remove"
                                                      style="color:#FF0004;"></span>
                                                1 цифра
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="input-lg form-control" tabindex="1"
                                                   name="password2"
                                                   id="password2"
                                                   placeholder="Подтвердите пароль" autocomplete="off" required>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                        <span id="pwmatch" class="glyphicon glyphicon-remove"
                                              style="color:#FF0004;"></span> Пароли совпадают
                                            </div>
                                        </div>
                                        <br>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3">
                                                    <input type="submit" tabindex="1" name="register-submit"
                                                           id="register-submit"
                                                           tabindex="4" class="form-control btn btn-register"
                                                           value="Зарегистрироваться" disabled>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<%@include file="footer.jsp" %>

<script>
    $("input[type=password]").keyup(function () {
        var ucase = new RegExp("[A-Z]+");
        var lcase = new RegExp("[a-z]+");
        var num = new RegExp("[0-9]+");

        if ($("#password1").val().length >= 8) {
            $("#8char").removeClass("glyphicon-remove");
            $("#8char").addClass("glyphicon-ok");
            $("#8char").css("color", "#00A41E");
        } else {
            $("#8char").removeClass("glyphicon-ok");
            $("#8char").addClass("glyphicon-remove");
            $("#8char").css("color", "#FF0004");
        }

        if (ucase.test($("#password1").val())) {
            $("#ucase").removeClass("glyphicon-remove");
            $("#ucase").addClass("glyphicon-ok");
            $("#ucase").css("color", "#00A41E");
        } else {
            $("#ucase").removeClass("glyphicon-ok");
            $("#ucase").addClass("glyphicon-remove");
            $("#ucase").css("color", "#FF0004");
        }

        if (lcase.test($("#password1").val())) {
            $("#lcase").removeClass("glyphicon-remove");
            $("#lcase").addClass("glyphicon-ok");
            $("#lcase").css("color", "#00A41E");
        } else {
            $("#lcase").removeClass("glyphicon-ok");
            $("#lcase").addClass("glyphicon-remove");
            $("#lcase").css("color", "#FF0004");
        }

        if (num.test($("#password1").val())) {
            $("#num").removeClass("glyphicon-remove");
            $("#num").addClass("glyphicon-ok");
            $("#num").css("color", "#00A41E");
        } else {
            $("#num").removeClass("glyphicon-ok");
            $("#num").addClass("glyphicon-remove");
            $("#num").css("color", "#FF0004");
        }

        if ($("#password1").val() == $("#password2").val() && $("#password1").val() != "") {
            $("#pwmatch").removeClass("glyphicon-remove");
            $("#pwmatch").addClass("glyphicon-ok");
            $("#pwmatch").css("color", "#00A41E");
            document.getElementById("register-submit").disabled = false;
        } else {
            $("#pwmatch").removeClass("glyphicon-ok");
            $("#pwmatch").addClass("glyphicon-remove");
            $("#pwmatch").css("color", "#FF0004");
            document.getElementById("register-submit").disabled = true;
        }
    });

    $(function () {
        $('#login-form-link').click(function q(e) {
            $("#login-form").delay(100).fadeIn(100);
            $("#register-form").fadeOut(100);
            $('#register-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
        $('#register-form-link').click(function (e) {
            $("#register-form").delay(100).fadeIn(100);
            $("#login-form").fadeOut(100);
            $('#login-form-link').removeClass('active');
            $(this).addClass('active');
            e.preventDefault();
        });
    });
</script>
<style>
    .panel-login {
        border-color: #ccc;
        -webkit-box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.2);
        box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.2);
    }

    .panel-login > .panel-heading {
        color: #00415d;
        background-color: #fff;
        border-color: #fff;
        text-align: center;
    }

    .panel-login > .panel-heading a {
        text-decoration: none;
        color: #666;
        font-weight: bold;
        font-size: 15px;
        -webkit-transition: all 0.1s linear;
        -moz-transition: all 0.1s linear;
        transition: all 0.1s linear;
    }

    .panel-login > .panel-heading a.active {
        color: #59B2E0;
        font-size: 18px;
    }

    .panel-login > .panel-heading hr {
        margin-top: 10px;
        margin-bottom: 0;
        clear: both;
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
        background-image: -moz-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
        background-image: -ms-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
        background-image: -o-linear-gradient(left, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.15), rgba(0, 0, 0, 0));
    }

    .panel-login input[type="text"], .panel-login input[type="email"], .panel-login input[type="password"] {
        height: 45px;
        border: 1px solid #ddd;
        font-size: 16px;
        -webkit-transition: all 0.1s linear;
        -moz-transition: all 0.1s linear;
        transition: all 0.1s linear;
    }

    .panel-login input:hover,
    .panel-login input:focus {
        outline: none;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        border-color: #ccc;
    }

    .btn-login {
        background-color: #59B2E0;
        outline: none;
        color: #fff;
        font-size: 14px;
        height: auto;
        font-weight: normal;
        padding: 14px 0;
        text-transform: uppercase;
        border-color: #59B2E6;
    }

    .btn-login:hover,
    .btn-login:focus {
        color: #fff;
        background-color: #53A3CD;
        border-color: #53A3CD;
    }

    .forgot-password {
        text-decoration: underline;
        color: #888;
    }

    .forgot-password:hover,
    .forgot-password:focus {
        text-decoration: underline;
        color: #666;
    }

    .btn-register {
        background-color: #59B2E0;
        outline: none;
        color: #fff;
        font-size: 14px;
        height: auto;
        font-weight: normal;
        padding: 14px 0;
        text-transform: uppercase;
    }

    .btn-register:hover,
    .btn-register:focus {
        color: #fff;
        background-color: #53A3CD;
        border-color: #53A3CD;
    }

    .btn-change-user {
        background-color: #59B2E0;
        outline: none;
        color: #fff;
        font-size: 13px;
        height: auto;
        font-weight: normal;
        padding: 13px 0;
        text-transform: uppercase;
    }

    .btn-change-user:hover,
    .btn-change-user:focus {
        color: #fff;
        background-color: #53A3CD;
        border-color: #53A3CD;
    }
</style>