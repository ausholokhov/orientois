<%--@elvariable id="exception" type="java.lang.Exception"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container">
    <h2>Произошла ошибка</h2>
    <div class="alert alert-danger">
        Произошла какая-то ошибка, попробуйте повторить свои действия или свяжитесь с системным администратором.
    </div>
    <div id="flip">Системная информация:</div>
    <div id="panel">
        <table width="100%" border="1">
            <tr valign="top">
                <td width="40%"><b>Error:</b></td>
                <td>${pageContext.exception}</td>
            </tr>
            <tr valign="top">
                <td><b>URI:</b></td>
                <td>${pageContext.errorData.requestURI}</td>
            </tr>
            <tr valign="top">
                <td><b>Status code:</b></td>
                <td>${pageContext.errorData.statusCode}</td>
            </tr>
            <tr valign="top">
                <td><b>Stack trace:</b></td>
                <td>
                    <c:forEach var="trace"
                               items="${pageContext.exception.stackTrace}">
                        <p>${trace}</p>
                    </c:forEach>
                </td>
            </tr>
        </table>
    </div>
</div>


<%@include file="footer.jsp" %>

<script>
    $(document).ready(function () {
        $("#flip").click(function () {
            $("#panel").slideToggle("slow");
        });
    });
</script>
<style>
    #panel, #flip {
        padding: 6px 12px;
        font-size: 14px;
        width: 100%;
        color: #555;
        background-color: #eeeeee;
        text-align: left;
        border: 1px solid #ccc;
        border-radius: 4px;
    }

    #panel {
        display: none;
    }
</style>