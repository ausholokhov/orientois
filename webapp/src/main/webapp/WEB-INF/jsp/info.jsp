<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--@elvariable id="employee" type="org.bitbucket.ausholokhov.orientois.models.Employee"--%>
<%--@elvariable id="employees" type="java.util.List<org.bitbucket.ausholokhov.orientois.models.Employee>"--%>

<%--Вывод найденых отрудников--%>

<c:if test="${employees!=null}">
    <c:choose>
        <c:when test="${employees.size()==0}">
            <br><h4 align="center">Совпадений не найдено.</h4>
        </c:when>
        <c:otherwise>
            <div class="container">
                <h3>Результат:</h3>
                <table class="table table-bordered">
                    <tr>
                        <td align="center"><strong>First name</strong></td>
                        <td align="center"><strong>Middle name</strong></td>
                        <td align="center"><strong>Last name</strong></td>
                        <td align="center"><strong>Email</strong></td>
                        <td align="center"><strong>Действия</strong></td>
                        <c:forEach var="employee" items="${employees}">
                    <tr>
                        <td align="center">${employee.firstname}</td>
                        <td align="center">${employee.middlename}</td>
                        <td align="center">${employee.lastname}</td>
                        <td align="center">${employee.email}</td>
                        <td align="center">
                            <button type="button" class="btn btn-info"
                                    onclick='self.location.href = "${pageContext.request.contextPath}/employeeInfo.htm?id=${employee.id}";'>
                                Подробная информация
                            </button>
                        </td>
                        </c:forEach>
                    </tr>
                </table>
            </div>
        </c:otherwise>
    </c:choose>
</c:if>

<style>
    .table-bordered > thead > tr > th,
    .table-bordered > tbody > tr > th,
    .table-bordered > tfoot > tr > th,
    .table-bordered > thead > tr > td,
    .table-bordered > tbody > tr > td,
    .table-bordered > tfoot > tr > td {
        border: 1px solid #8c8c8c;
    }
</style>