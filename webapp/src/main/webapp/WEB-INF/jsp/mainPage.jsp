<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--Главная страница--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <c:choose>
                <c:when test="${news.size()==0 || news==null}">
                    <h3 align="center">Новостей пока нет</h3>
                </c:when>
                <c:otherwise>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <c:forEach var="index" begin="1" end="${news.size()-1}">
                                <li data-target="#carousel-example-generic" data-slide-to="${index}"></li>
                            </c:forEach>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="data:image/gif;base64,${news.get(0).image}" alt="First slide">
                                <div class="carousel-caption">
                                    <c:choose>
                                        <c:when test="${news.get(0)!=null}">
                                            <h3>${news.get(0).header}</h3>
                                            <p>${news.get(0).body}</p>
                                        </c:when>
                                        <c:otherwise>
                                            <h3>Новостей пока нет</h3>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </div>
                            <c:forEach var="index" begin="1" end="${news.size()-1}">
                                <div class="item">
                                    <img src="data:image/gif;base64,${news.get(index).image}">
                                    <div class="carousel-caption">
                                        <h3>${news.get(index).header}</h3>
                                        <p>${news.get(index).body}</p>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span></a><a class="right carousel-control"
                                                                                         href="#carousel-example-generic"
                                                                                         data-slide="next"><span
                            class="glyphicon glyphicon-chevron-right">
                        </span></a>
                    </div>
                    <div class="main-text hidden-xs">
                        <div class="col-md-12 text-center">
                            <h3>Новостная лента</h3>
                        </div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</div>
<div id="push">
</div>


<%@include file="footer.jsp" %>

<style>
    .main-text {
        position: absolute;
        top: 20px;
        width: 96.9666666666666%;
        color: #FFF;
    }

    .carousel-caption {
        position: absolute;
        top: 40px;
    }
</style>