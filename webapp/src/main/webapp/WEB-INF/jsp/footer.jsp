<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="wrap">
    <div rel="main"></div>
</div>
<footer class="container-fluid text-center">
    <p>Sholokhov Anton OrientOIS</p>
</footer>
</body>
</html>
<style>
    html,
    body {
        width: 100%;
        background-color: #f8f8f8;
    }

    .wrap {
        width: 80%;
        margin: auto;
        background-color: #f8f8f8;
    }

    footer {
        background-color: #666;
        width: 80%;
        margin: auto;
    }
    html,
    body {
        height: 100%;
    }

    .wrap {
        min-height: 100%;
    }

    [rel="main"] {
        padding-bottom: 100px;
    }

    footer {
        position: relative;
        left: 0;
        bottom: 0;
        background-color: #555;
        color: white;
        padding: 15px;
        width: 100%;
    }
</style>