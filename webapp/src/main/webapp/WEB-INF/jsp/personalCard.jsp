<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--Карточка сотрудника--%>
<!DOCTYPE html>
<html lang="en">
<%@include file="headerAndNavbar.jsp" %>


<div class="container">
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Напишите текст заметки</h4>
                </div>
                <div class="modal-body">
                    <textarea id="stickerText" class="form-control" rows="3" title=""></textarea>
                    <input type='text' class="basic"/>
                    <input type='hidden' id="stickerColor"/>
                </div>
                <div class="modal-footer">
                    <button id="modalSticker" type="button" class="btn btn-success" data-dismiss="modal">Добавить
                    </button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-2 sidenav">
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a data-toggle="pill" href="#card">Карточка</a></li>
                <li><a data-toggle="pill" href="#resume">Резюме</a></li>
            </ul>
        </div>
        <div class="col-sm-8 text-left">
            <div class="container">
                <div class="tab-content">
                    <div id="card" class="tab-pane fade in active">

                        <h3 align="center">Карточка</h3>
                        <div class="container">
                            <div class="row">
                                <article>
                                    <aside>
                                        <div class="card hovercard">
                                            <form id="imageUploadForm" method="POST"
                                                  action="<c:url value="/doUpload" />"
                                                  enctype="multipart/form-data">
                                                <input type="hidden" name="id" value="${employee.personalCard.id}">
                                                <input type="hidden" name="employee.id" value="${employee.id}">
                                           <c:if test="${employee.id == sessionScope.get('sessionEmployee').id}">
                                            <span class="btn btn-info btn-file">
                                                 <span class="glyphicon glyphicon-folder-open" aria-hidden="true">
                                                 </span><input type="file" id="imgInp" name="file"
                                                               accept="image/jpeg, image/png">
                                             </span>
                                            <span class="btn btn-warning btn-file" id="restoreS">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true">
                                                </span><input type="button" id="restore"/>
                                            </span>
                                            <span class="btn btn-success btn-file" id="saveImageS">
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                                </span><input type="submit" id="saveImage"/>
                                            </span>
                                           </c:if>

                                            </form>
                                            <img id="avatarhead" class="cardheader"
                                                 src="data:image/gif;base64,${employee.personalCard.photo}"
                                                 alt="avatar"/>
                                            <div class="avatar">
                                                <img id="avatar"
                                                     src="data:image/gif;base64,${employee.personalCard.photo}"
                                                     alt="no image available"/>
                                            </div>
                                            <div class="info">
                                                <div class="title">
                                                    <a href="<c:url value="/employeeInfo?id=${employee.id}"/>">${employee.firstname}&nbsp${employee.lastname}</a>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-info" data-toggle="modal"
                                                    data-target="#myModal">Оставить стикер
                                            </button>
                                        </div>
                                    </aside>
                                    <form:form method="post" id="postForm" name="postForm" role="form">
                                        <input type="hidden" name="id" id="id" value="${employee.personalCard.id}">
                                        <div class="col-sm-10" id="postDiv">
                                            <article>
                                                <aside>
                                                    <h4 align="rigth">Что я думаю:</h4>
                                                </aside>
                                                <div align="right">
                                                 <span class="btn btn-success btn-file" id="savePostS">
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true">
                                                </span><input type="button" id="savePost"/>
                                            </span>
                                                    <c:if test="${employee.id == sessionScope.get('sessionEmployee').id}">
                                                    <span class="btn btn-warning btn-file" id="editPostS">
                                                <span class="glyphicon glyphicon-edit" aria-hidden="true">
                                                </span><input type="button" id="editPost"/>
                                            </span>
                                                    </c:if>
                                                </div>
                                            </article>
                                            <textarea disabled class="form-control vresize" rows="8"
                                                      name="post"
                                                      id="post"
                                                      title="">${employee.personalCard.post}</textarea>
                                            <c:forEach items="${employee.personalCard.stickers}" var="sticker">
                                                <div id="${sticker.id}" class='ui-widget-content'
                                                     style="${sticker.position}" onmouseup=saveSticker(this);>
                                                    <i class='glyphicon glyphicon-remove'
                                                       onclick=stickerDelete(this.parentNode);></i>
                                                    <p>${sticker.message}</p>
                                                    <div class='stickerFoot'>${sticker.author}
                                                    </div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </form:form>
                                </article>
                            </div>
                        </div>
                    </div>
                    <form action="<c:url value="/downloadPDF"/>" id="resumeId" method="post">
                        <input type="hidden" name="id" value="${employee.id}">
                    </form>

                    <div id="resume" class="tab-pane fade">
                        <h3 align="center">Резюме</h3>

                        <h4 align="center">${employee.lastname}&nbsp${employee.firstname}&nbsp${employee.middlename}</h4>

                        <div align="right">
                            <button type="submit" class="btn btn-info" id="saveToPdf" form="resumeId">
                                <i class="glyphicon glyphicon-save"></i> Сохранить в PDF
                            </button>
                            <c:if test="${employee.id == sessionScope.get('sessionEmployee').id}">
                            <button type="button" class="btn btn-success" id="resumeSave" form="resumeForm">
                                <i class="glyphicon glyphicon-ok"></i> Сохранить
                            </button>
                            <button type="button" class="btn btn-warning" id="resumeEdit" onclick="fieldsetEnabled()">
                                <i class="glyphicon glyphicon-pencil"></i> Редактировать
                            </button>
                            </c:if>
                        </div>

                        <form:form name="resumeForm" id="resumeForm" class="form-horizontal" role="form" method="post">
                            <input type="hidden" name="id" id="id" value="${employee.resume.id}">
                            <input type="hidden" name="fullname" id="fullname"
                                   value="${employee.lastname}&nbsp${employee.firstname}&nbsp${employee.middlename}">
                            <fieldset disabled id="resumeFieldset">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">Дата рождения:</label>

                                    <div class="col-sm-10">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" title="" data-provide="datepicker"
                                                   data-date-format="yyyy-mm-dd"
                                                   value="${employee.resume.birthdate}" id="birthdate"
                                                   name="birthdate">
                                            <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-th"></i></span>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label">Основное образование:</label>

                                    <div class="col-sm-10">
                                            <textarea class="form-control vresize" rows="5"
                                                      name="mainEducation"
                                                      id="mainEducation"
                                                      title="">${employee.resume.mainEducation}</textarea>
                                    </div>

                                    <label class="col-sm-2 control-label">Дополнительное образование:</label>

                                    <div class="col-sm-10">
                                            <textarea class="form-control vresize" rows="5" name="additionalEducation"
                                                      id="additionalEducation"
                                                      title="">${employee.resume.additionalEducation}</textarea>
                                    </div>

                                    <label class="col-sm-2 control-label">Предыдущее место работы:</label>

                                    <div class="col-sm-10">
                                            <textarea class="form-control vresize" rows="5" name="priorWorkplace"
                                                      id="priorWorkplace"
                                                      title="">${employee.resume.priorWorkplace}</textarea>
                                    </div>

                                    <label class="col-sm-2 control-label">Профессиональный опыт</label>

                                    <div class="col-sm-10">
                                             <textarea class="form-control vresize" rows="5"
                                                       name="professionalExpirience"
                                                       id="professionalExpirience"
                                                       title="">${employee.resume.professionalExpirience}</textarea>
                                    </div>

                                    <label class="col-sm-2 control-label">Дополнительные навыки</label>

                                    <div class="col-sm-10">
                                             <textarea class="form-control vresize" rows="5" name="skills"
                                                       id="skills"
                                                       title="">${employee.resume.skills}</textarea>
                                    </div>

                                    <div>&nbsp</div>
                                    <label class="col-sm-2 control-label">Есть дети?</label>

                                    <div class="[ form-group ]" style="padding-left: 20%">
                                        <input type="checkbox" name="hasChildren" id="hasChildren"
                                               onclick="checkBox(hasChildren)"/>

                                        <div class="[ btn-group ]">
                                            <label for="hasChildren" class="[ btn btn-info ]">
                                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                                <span> </span>
                                            </label>
                                            <label for="hasChildren" class="[ btn btn-default active ]">
                                                Нажмите для подтверждения
                                            </label>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label">Готовы к переезду?</label>

                                    <div class="[ form-group ]" style="padding-left: 20%">
                                        <input type="checkbox" name="readyForRelocation" id="readyForRelocation"
                                               onclick="checkBox(readyForRelocation)"/>

                                        <div class="[ btn-group ]">
                                            <label for="readyForRelocation" class="[ btn btn-info ]">
                                                <span class="[ glyphicon glyphicon-ok ]"></span>
                                                <span> </span>
                                            </label>
                                            <label for="readyForRelocation" class="[ btn btn-default active ]">
                                                Нажмите для подтверждения
                                            </label>
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label">О себе</label>

                                    <div class="col-sm-10">
                                             <textarea class="form-control vresize" rows="5" name="additionalData"
                                                       id="additionalData"
                                                       title="">${employee.resume.additionalData}</textarea>
                                    </div>

                                    <label class="col-sm-2 control-label">Контактная информация</label>

                                    <div class="col-sm-10">
                                             <textarea class="form-control vresize" rows="5" name="contactInfo"
                                                       id="contactInfo"
                                                       title="">${employee.resume.contactInfo}</textarea>
                                    </div>
                                </div>
                            </fieldset>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-2 sidenav">
            <div class="well">
                <p>reserved</p>
            </div>
            <div class="well">
                <p>reserved</p>
            </div>
        </div>
    </div>
</div>
<c:set var="authorName"
       value="${sessionScope.get('sessionEmployee').lastname} ${sessionScope.get('sessionEmployee').firstname}"/>

<%@include file="footer.jsp" %>

<script>
    $(".basic").spectrum({
        color: "#ffe580",
        change: function (color) {
            $("#stickerColor").attr('value', color.toHexString());
        }
    });

    function stickerDelete(stickerID) {
        var sticker = document.getElementById(stickerID.id);
        var id = sticker.getAttribute("id");
        $.ajax({
            url: '<c:url value="/deleteSticker"/>',
            data: {
                id: id,
                userId: ${sessionScope.get("sessionEmployee").id}
            }, success: function () {
                sticker.remove();
            }
        });
    }

    $("#modalSticker").click(function () {
        var text = $("#stickerText").val();
        var div = $("#postDiv");
        var color = $("#stickerColor").val();
        var element = "<div id ='stickerTempID'  class='ui-widget-content' onmouseup=saveSticker(this)> <i class='glyphicon glyphicon-remove' onclick=stickerDelete(this.parentNode)></i> <p>" + text + "</p> <div class='stickerFoot'>${sessionScope.get("sessionEmployee").lastname}&nbsp${sessionScope.get("sessionEmployee").firstname}</div>";
        div.append(element);
        $(".ui-widget-content").draggable();
        var sticker = $("#stickerTempID");
        sticker.css("background-color", '\"' + color + '\"');
        var name = "${authorName}";
        $.ajax({
            url: '<c:url value="/saveSticker"/>',
            data: {
                position: sticker.attr("style"),
                author: name,
                cardId: ${employee.personalCard.id},
                userId: ${sessionScope.get("sessionEmployee").id},
                message: sticker.find('p').text()
            }, success: function (data) {
                sticker.attr("id", data);
            }
        });
    });

    function saveSticker(sticker) {
        var position = sticker.getAttribute("style");
        var id = sticker.getAttribute("id");
        var message = sticker.getElementsByTagName("p")[0].innerHTML;
        var name = "${authorName}";
        $.ajax({
            url: '<c:url value="/saveSticker"/>',
            data: {
                position: position,
                id: id,
                author: name,
                cardId: ${employee.personalCard.id},
                userId: ${sessionScope.get("sessionEmployee").id},
                message: message
            }, success: function (data) {
                sticker.setAttribute("id", data);
            }
        });
    }

    $(function () {
        $(".ui-widget-content").draggable();
    });

    function successAlert() {
        swal({
            title: 'Сохранено',
            text: '',
            timer: 1000,
            showConfirmButton: false,
            animation: true,
            type: 'success'
        });
    }


    $("#editPost").click(function () {
        $("#post").attr("disabled", false);
        $("#savePostS").show();
        $("#editPostS").hide();
    });

    var srcAvatarImage = $('#avatar').attr('src');
    var srcAvatarHead = $('#avatarhead').attr('src');
    $('#imageUploadForm').submit(function (evt) {
        evt.preventDefault();
        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function () {
                successAlert();
                $("#restoreS").hide();
                $("#saveImageS").hide();
                srcAvatarImage = $('#avatar').attr('src');
                srcAvatarHead = $('#avatarhead').attr('src');
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        });
    });


    $("#savePost").click(function () {
        swal($.ajax({
            type: "POST",
            url: '<c:url value="/savePost"/>',
            data: $("#postForm").serialize(),
            dataType: 'text',
            timeout: 100000,
            success: function () {
                successAlert();
                $("#post").attr("disabled", true);
                $("#savePostS").hide();
                $("#editPostS").show();
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        }))
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#avatar').attr('src', e.target.result);
                $('#avatarhead').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#restore").click(function () {
        $("#restoreS").hide();
        $("#saveImageS").hide();
        restore();
    });

    $("#imgInp").change(function () {
        if (this.files[0].size / 1024 / 1024 > 2) {
            swal('Фото должно весить не больше 2 мегабайт.', '', 'error');
            restore();
        } else {
            readURL(this);
            $("#restoreS").show();
            $("#saveImageS").show();
        }
    });

    function restore() {
        $("#avatar").attr('src', srcAvatarImage);
        $("#avatarhead").attr('src', srcAvatarImage);
    }


    $("#resumeSave").click(function () {
        swal($.ajax({
            type: "POST",
            url: '<c:url value="/resumeSave"/>',
            data: $("#resumeForm").serialize(),
            dataType: 'text',
            timeout: 100000,
            success: function () {
                successAlert();
                fieldsetEnabled();
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        }))
    });

    $(document).ready(function () {
        $("#restoreS").hide();
        $("#saveImageS").hide();
        $("#resumeSave").hide();
        $("#savePostS").hide();
        $("#birthdate").datepicker("option", "disabled", true);
        var hasChildren = ${employee.resume.hasChildren};
        var readyForRelocation = ${employee.resume.readyForRelocation};
        if (hasChildren) {
            $("#hasChildren").prop("checked", true);
        }
        if (readyForRelocation) {
            $("#readyForRelocation").prop("checked", true);
        }
    });

    function checkBox(cbox) {
        document.getElementById(cbox).checked = document.getElementById(cbox).checked != true;
    }

    var fsdisabled = true;
    function fieldsetEnabled() {
        var dynamicEditButton = document.getElementById("resumeEdit");
        var dynamicFieldSet = document.getElementById("resumeFieldset");
        if (fsdisabled) {
            dynamicFieldSet.disabled = fsdisabled = false;
            dynamicEditButton.innerHTML = "Отмена";
            $("#resumeSave").show();
            $("#saveToPdf").hide();
        } else {
            dynamicFieldSet.disabled = fsdisabled = true;
            dynamicEditButton.innerHTML = "Редактировать";
            $("#resumeSave").hide();
            $("#saveToPdf").show();
        }
    }
</script>

<style>
    .stickerFoot {
        position: absolute;
        bottom: 0;
        left: 5px;
        width: 150px;
        font-size: 12px;
    }

    .glyphicon-remove {

        float: right;
    }

    .glyphicon-remove:hover {
        color: white;
    }

    .ui-widget-content {
        width: 180px;
        height: 180px;
        padding: 0.5em;
        background: #ffe580;
    }

    article {
        position: relative;
        display: block;
        width: 75%;
        left: 25%;
    }

    aside {
        position: absolute;
        display: block;
        left: -33%;
        width: 33%;
        height: 100%;
    }

    textarea {
        resize: none;
    }

    .card {
        padding-top: 20px;
        margin: 10px 0 20px 0;
        background-color: rgba(214, 224, 226, 0.2);
        border-top-width: 0;
        border-bottom-width: 2px;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .card .card-heading.image img {
        display: inline-block;
        width: 46px;
        height: 46px;
        margin-right: 15px;
        vertical-align: top;
        border: 0;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
    }

    .card .card-heading.image .card-heading-header h3 {
        margin: 0;
        font-size: 14px;
        line-height: 16px;
        color: #262626;
    }

    .card .card-heading.image .card-heading-header span {
        font-size: 12px;
        color: #999999;
    }

    .card .card-media img {
        max-width: 100%;
        max-height: 100%;
    }

    .card .card-comments .comments-collapse-toggle a,
    .card .card-comments .comments-collapse-toggle span {
        padding-right: 5px;
        overflow: hidden;
        font-size: 12px;
        color: #999;
        text-overflow: ellipsis;
        white-space: nowrap;
    }

    .card.people .card-info .title {
        display: block;
        margin: 8px 14px 0 14px;
        overflow: hidden;
        font-size: 16px;
        font-weight: bold;
        line-height: 18px;
        color: #404040;
    }

    .card.hovercard {
        position: relative;
        padding-top: 0;
        overflow: hidden;
        text-align: center;
        background-color: rgba(214, 224, 226, 0.2);
    }

    .card.hovercard .cardheader {
        width: 100%;
        background-size: 100%;
        height: 70px;
        filter: blur(15px);
        -webkit-filter: blur(15px);
        -mox-filter: blur(15px);
    }

    .card.hovercard .avatar {
        position: relative;
        top: -50px;
        margin-bottom: -50px;
    }

    .card.hovercard .avatar img {
        width: 130px;
        height: 130px;
        max-width: 130px;
        max-height: 130px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        border: 5px solid rgba(255, 255, 255, 0.5);
    }

    .card.hovercard .info {
        padding: 4px 8px 10px;
    }

    .card.hovercard .info .title {
        margin-bottom: 4px;
        font-size: 24px;
        line-height: 1;
        color: #262626;
        vertical-align: middle;
    }

    .btn-file {
        position: relative;
        overflow: hidden;
    }

    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    .btn-file input[type=submit] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    .btn-file input[type=button] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }

    .form-group input[type="checkbox"] {
        display: none;
    }

    .form-group input[type="checkbox"] + .btn-group > label span {
        width: 20px;
    }

    .form-group input[type="checkbox"] + .btn-group > label span:first-child {
        display: none;
    }

    .form-group input[type="checkbox"] + .btn-group > label span:last-child {
        display: inline-block;
    }

    .form-group input[type="checkbox"]:checked + .btn-group > label span:first-child {
        display: inline-block;
    }

    .form-group input[type="checkbox"]:checked + .btn-group > label span:last-child {
        display: none;
    }

    textarea {
        min-height: 110px;
    }

    .vresize {
        resize: vertical;
    }

</style>