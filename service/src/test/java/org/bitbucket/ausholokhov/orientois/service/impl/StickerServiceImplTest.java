package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.StickerDao;
import org.bitbucket.ausholokhov.orientois.models.Sticker;
import org.bitbucket.ausholokhov.orientois.service.StickerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class StickerServiceImplTest {

    private StickerDao dao;
    private StickerService service;

    @Before
    public void doBefore() {
        dao = mock(StickerDao.class);
        service = new StickerServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        Sticker sticker = new Sticker();
        when(dao.add(sticker)).thenReturn(sticker);
        Sticker result = service.add(sticker);
        verify(dao).add(sticker);
        Assert.assertEquals(sticker, result);
    }

    @Test
    public void testGet() throws Exception {
        Sticker sticker = new Sticker();
        when(dao.get(anyLong())).thenReturn(sticker);
        Sticker result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(sticker, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        Sticker sticker = new Sticker();
        service.update(sticker);
        verify(dao).update(sticker);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Sticker> stickers = new ArrayList<>();
        stickers.add(new Sticker());
        stickers.add(new Sticker());
        when(dao.getAll()).thenReturn(stickers);
        List<Sticker> result = service.getAll();
        Assert.assertEquals(result.get(0), stickers.get(0));
    }

}