package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.DepartmentDao;
import org.bitbucket.ausholokhov.orientois.models.Department;
import org.bitbucket.ausholokhov.orientois.service.DepartmentService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class DepartmentServiceImplTest {

    private DepartmentDao dao;
    private DepartmentService service;

    @Before
    public void doBefore() {
        dao = mock(DepartmentDao.class);
        service = new DepartmentServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        Department department = new Department();
        when(dao.add(department)).thenReturn(department);
        Department result = service.add(department);
        verify(dao).add(department);
        Assert.assertEquals(department, result);
    }

    @Test
    public void testGet() throws Exception {
        Department department = new Department();
        when(dao.get(anyLong())).thenReturn(department);
        Department result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(department, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        Department department = new Department();
        service.update(department);
        verify(dao).update(department);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Department> departments = new ArrayList<>();
        departments.add(new Department());
        departments.add(new Department());
        when(dao.getAll()).thenReturn(departments);
        List<Department> result = service.getAll();
        Assert.assertEquals(result.get(0), departments.get(0));
    }
}