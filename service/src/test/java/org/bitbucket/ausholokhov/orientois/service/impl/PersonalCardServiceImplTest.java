package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.PersonalCardDao;
import org.bitbucket.ausholokhov.orientois.models.PersonalCard;
import org.bitbucket.ausholokhov.orientois.service.PersonalCardService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class PersonalCardServiceImplTest {

    private PersonalCardDao dao;
    private PersonalCardService service;

    @Before
    public void doBefore() {
        dao = mock(PersonalCardDao.class);
        service = new PersonalCardServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        PersonalCard card = new PersonalCard();
        when(dao.add(card)).thenReturn(card);
        PersonalCard result = service.add(card);
        verify(dao).add(card);
        Assert.assertEquals(card, result);
    }

    @Test
    public void testGet() throws Exception {
        PersonalCard card = new PersonalCard();
        when(dao.get(anyLong())).thenReturn(card);
        PersonalCard result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(card, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        PersonalCard card = new PersonalCard();
        service.update(card);
        verify(dao).update(card);
    }

    @Test
    public void testGetAll() throws Exception {
        List<PersonalCard> cards = new ArrayList<>();
        cards.add(new PersonalCard());
        cards.add(new PersonalCard());
        when(dao.getAll()).thenReturn(cards);
        List<PersonalCard> result = service.getAll();
        Assert.assertEquals(result.get(0), cards.get(0));
    }
}