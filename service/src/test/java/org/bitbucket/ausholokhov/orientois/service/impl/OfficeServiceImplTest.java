package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.OfficeDao;
import org.bitbucket.ausholokhov.orientois.models.Department;
import org.bitbucket.ausholokhov.orientois.models.Office;
import org.bitbucket.ausholokhov.orientois.models.dto.OfficeDto;
import org.bitbucket.ausholokhov.orientois.service.OfficeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class OfficeServiceImplTest {

    private OfficeDao dao;
    private OfficeService service;

    @Before
    public void doBefore() {
        dao = mock(OfficeDao.class);
        service = new OfficeServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        Office office = new Office();
        when(dao.add(office)).thenReturn(office);
        Office result = service.add(office);
        verify(dao).add(office);
        Assert.assertEquals(office, result);
    }

    @Test
    public void testGet() throws Exception {
        Office office = new Office();
        when(dao.get(anyLong())).thenReturn(office);
        Office result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(office, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        Office office = new Office();
        service.update(office);
        verify(dao).update(office);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Office> offices = new ArrayList<>();
        offices.add(new Office());
        offices.add(new Office());
        when(dao.getAll()).thenReturn(offices);
        List<Office> result = service.getAll();
        Assert.assertEquals(result.get(0), offices.get(0));
    }

    @Test
    public void testSearchOfficeDtoByCriteria() throws Exception {
        List<Office> offices = new ArrayList<>();
        offices.add(new Office("testOffice1"));
        offices.add(new Office("testOffice2"));
        when(dao.getAll()).thenReturn(offices);
        List<OfficeDto> result = service.searchOfficeDtoByCriteria("1");
        verify(dao).getAll();
        Assert.assertEquals(result.get(0).name, "testOffice1");
    }
}