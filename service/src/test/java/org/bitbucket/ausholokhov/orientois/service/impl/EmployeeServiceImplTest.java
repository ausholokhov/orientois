package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.EmployeeDao;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class EmployeeServiceImplTest {

    private EmployeeDao dao;
    private EmployeeService service;

    @Before
    public void doBefore() {
        dao = mock(EmployeeDao.class);
        service = new EmployeeServiceImpl(dao);
    }


    @Test
    public void testAdd() throws Exception {
        Employee employee = new Employee();
        when(dao.add(employee)).thenReturn(employee);
        Employee result = service.add(employee);
        verify(dao).add(employee);
        Assert.assertEquals(employee, result);
    }

    @Test
    public void testGet() throws Exception {
        Employee employee = new Employee();
        when(dao.get(anyLong())).thenReturn(employee);
        Employee result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(employee, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        Employee employee = new Employee();
        service.update(employee);
        verify(dao).update(employee);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee());
        employees.add(new Employee());
        when(dao.getAll()).thenReturn(employees);
        List<Employee> result = service.getAll();
        Assert.assertEquals(result.get(0), employees.get(0));
    }

    @Test
    public void testCriteriaFilter() throws Exception {
        List<Employee> employees = new ArrayList<>();
        Employee employeeFirst = new Employee();
        employeeFirst.setFirstname("Employee1");
        Employee employeeSecond = new Employee();
        employeeSecond.setFirstname("Employee2");
        employees.add(employeeFirst);
        employees.add(employeeSecond);
        List<Employee> result = service.criteriaFilter(employees, "1");
        Assert.assertEquals(result.get(0), employeeFirst);
    }

    @Test
    public void testGetByOffice() throws Exception {
        service.getByOffice(anyLong());
        verify(dao).getByOffice(anyLong());
    }

    @Test
    public void testGetEmployeesByCriteria() throws Exception {
        service.getEmployeesByCriteria(anyString());
        verify(dao).getEmployeesByCriteria(anyString());
    }

    @Test
    public void testGetEmployeeByAccount() throws Exception {
        Account account = new Account("1", "1");
        Employee employee = new Employee();
        employee.setAccount(account);
        when(dao.getEmployeeByAccount(account)).thenReturn(employee);
        Employee result = service.getEmployeeByAccount(account);
        verify(dao).getEmployeeByAccount(account);
        Assert.assertEquals(employee, result);
    }
}