package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.ResumeDao;
import org.bitbucket.ausholokhov.orientois.models.Resume;
import org.bitbucket.ausholokhov.orientois.service.ResumeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class ResumeServiceImplTest {

    private ResumeDao dao;
    private ResumeService service;

    @Before
    public void doBefore() {
        dao = mock(ResumeDao.class);
        service = new ResumeServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        Resume resume = new Resume();
        when(dao.add(resume)).thenReturn(resume);
        Resume result = service.add(resume);
        verify(dao).add(resume);
        Assert.assertEquals(resume, result);
    }

    @Test
    public void testGet() throws Exception {
        Resume resume = new Resume();
        when(dao.get(anyLong())).thenReturn(resume);
        Resume result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(resume, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        Resume resume = new Resume();
        service.update(resume);
        verify(dao).update(resume);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Resume> resumes = new ArrayList<>();
        resumes.add(new Resume());
        resumes.add(new Resume());
        when(dao.getAll()).thenReturn(resumes);
        List<Resume> result = service.getAll();
        Assert.assertEquals(result.get(0), resumes.get(0));
    }
}