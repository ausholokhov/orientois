package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.NewsDao;
import org.bitbucket.ausholokhov.orientois.models.News;
import org.bitbucket.ausholokhov.orientois.service.NewsService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

/**
 * Created by Anton on 14.02.2016.
 */
public class NewsServiceImplTest {

    private NewsDao dao;
    private NewsService service;

    @Before
    public void doBefore() {
        dao = mock(NewsDao.class);
        service = new NewsServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        News news = new News();
        when(dao.add(news)).thenReturn(news);
        News result = service.add(news);
        verify(dao).add(news);
        Assert.assertEquals(news, result);
    }

    @Test
    public void testGet() throws Exception {
        News news = new News();
        when(dao.get(anyLong())).thenReturn(news);
        News result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(news, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        News news = new News();
        service.update(news);
        verify(dao).update(news);
    }

    @Test
    public void testGetAll() throws Exception {
        List<News> newses = new ArrayList<>();
        newses.add(new News());
        newses.add(new News());
        when(dao.getAll()).thenReturn(newses);
        List<News> result = service.getAll();
        Assert.assertEquals(result.get(0), newses.get(0));
    }
}