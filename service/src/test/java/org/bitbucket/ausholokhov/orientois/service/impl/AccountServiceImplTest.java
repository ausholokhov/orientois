package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.AccountDao;
import org.bitbucket.ausholokhov.orientois.dao.impl.AccountDaoImpl;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.service.AccountService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by Anton on 13.02.2016.
 */
public class AccountServiceImplTest {

    private AccountDao dao;
    private AccountService service;

    @Before
    public void doBefore() {
        dao = mock(AccountDaoImpl.class);
        service = new AccountServiceImpl(dao);
    }

    @Test
    public void testAdd() throws Exception {
        Account account = new Account("1", "2");
        when(dao.add(account)).thenReturn(account);
        Account result = service.add(account);
        verify(dao).add(account);
        Assert.assertEquals(account, result);
    }

    @Test
    public void testGet() throws Exception {
        Account account = new Account("1", "2");
        when(dao.get(anyLong())).thenReturn(account);
        Account result = service.get(anyLong());
        verify(dao).get(anyLong());
        Assert.assertEquals(account, result);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(anyLong());
        verify(dao).delete(anyLong());
    }

    @Test
    public void testUpdate() throws Exception {
        Account account = new Account("1","1");
        service.update(account);
        verify(dao).update(account);
    }

    @Test
    public void testGetAll() throws Exception {
        List<Account> accounts = new ArrayList<>();
        accounts.add(new Account("1", "1"));
        accounts.add(new Account("2", "2"));
        when(dao.getAll()).thenReturn(accounts);
        List<Account> result = service.getAll();
        Assert.assertEquals(result.get(0).getLogin(), "1");
    }

    @Test
    public void testGetByLogin() throws Exception {
        Account account = new Account("1", "2");
        when(dao.getByLogin("1")).thenReturn(account);
        Account result = service.getByLogin("1");
        verify(dao).getByLogin("1");
        Assert.assertEquals(account, result);
    }
}