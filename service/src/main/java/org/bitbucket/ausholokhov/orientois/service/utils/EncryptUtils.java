package org.bitbucket.ausholokhov.orientois.service.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;

/**
 * Created by Anton on 18.01.2016.
 */
public class EncryptUtils {

    private static StrongPasswordEncryptor passwordEncryptor;
    private static StandardPBEStringEncryptor textEncryptor;
    private static final String ALGORITHM = "PBEWithMD5AndTripleDES";
    private static final String TEXT_ENCRYPTOR_PASSWORD = "ABXY";

    static {
        passwordEncryptor = new StrongPasswordEncryptor();
        textEncryptor = new StandardPBEStringEncryptor();
        textEncryptor.setAlgorithm(ALGORITHM);
        textEncryptor.setPassword(TEXT_ENCRYPTOR_PASSWORD);
    }

    public static String encryptPassword(String password) {
        return passwordEncryptor.encryptPassword(password);
    }

    public static boolean checkPassword(String enteredPassword, String accountPassword) {
        return passwordEncryptor.checkPassword(enteredPassword, accountPassword);
    }

    public static String encryptText(String text) {
        return textEncryptor.encrypt(text);
    }

    public static String decryptText(String text) {
        return textEncryptor.decrypt(text);
    }
}
