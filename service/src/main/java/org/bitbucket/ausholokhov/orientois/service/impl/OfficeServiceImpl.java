package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.OfficeDao;
import org.bitbucket.ausholokhov.orientois.models.Office;
import org.bitbucket.ausholokhov.orientois.models.dto.OfficeDto;
import org.bitbucket.ausholokhov.orientois.service.OfficeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Service
public class OfficeServiceImpl implements OfficeService {

    private OfficeDao dao;

    public OfficeServiceImpl() {
    }

    @Autowired
    public OfficeServiceImpl(OfficeDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public Office add(Office o) {
        return dao.add(o);
    }

    @Override
    public Office get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(Office o) {
        dao.update(o);
    }

    @Override
    public List<Office> getAll() {
        return dao.getAll();
    }

    @Override
    public List<OfficeDto> searchOfficeDtoByCriteria(String criteria) {
        List<Office> offices = dao.getAll();
        List<OfficeDto> officesDto = new LinkedList<>();
        for (Office office : offices) {
            if (office.getName().toLowerCase().contains(criteria.toLowerCase())) {
                OfficeDto officeDto = new OfficeDto();
                officeDto.id = office.getId();
                officeDto.name = office.getName();
                officesDto.add(officeDto);
            }
        }
        return officesDto;
    }
}
