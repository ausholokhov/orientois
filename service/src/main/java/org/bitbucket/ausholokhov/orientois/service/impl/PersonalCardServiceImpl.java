package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.PersonalCardDao;
import org.bitbucket.ausholokhov.orientois.models.PersonalCard;
import org.bitbucket.ausholokhov.orientois.service.PersonalCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Service
public class PersonalCardServiceImpl implements PersonalCardService {

    private PersonalCardDao dao;

    public PersonalCardServiceImpl() {
    }

    @Autowired
    public PersonalCardServiceImpl(PersonalCardDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public PersonalCard add(PersonalCard p) {
        return dao.add(p);
    }

    @Override
    public PersonalCard get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(PersonalCard p) {
        dao.update(p);
    }

    @Override
    public List<PersonalCard> getAll() {
        return dao.getAll();
    }

}
