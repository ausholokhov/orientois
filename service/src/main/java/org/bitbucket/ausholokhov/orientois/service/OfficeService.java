package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.Office;
import org.bitbucket.ausholokhov.orientois.models.dto.OfficeDto;

import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
public interface OfficeService extends CrudService<Office> {
    List<OfficeDto> searchOfficeDtoByCriteria(String criteria);
}
