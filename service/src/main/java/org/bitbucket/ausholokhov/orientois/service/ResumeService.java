package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.Resume;

/**
 * Created by Anton on 21.11.2015.
 */
public interface ResumeService extends CrudService<Resume> {
}
