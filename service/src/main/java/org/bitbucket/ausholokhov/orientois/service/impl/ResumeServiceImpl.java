package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.ResumeDao;
import org.bitbucket.ausholokhov.orientois.models.Resume;
import org.bitbucket.ausholokhov.orientois.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Service
public class ResumeServiceImpl implements ResumeService {

    private ResumeDao dao;

    public ResumeServiceImpl() {
    }

    @Autowired
    public ResumeServiceImpl(ResumeDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public Resume add(Resume r) {
        return dao.add(r);
    }

    @Override
    public Resume get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(Resume r) {
        dao.update(r);
    }

    @Override
    public List<Resume> getAll() {
        return dao.getAll();
    }
}
