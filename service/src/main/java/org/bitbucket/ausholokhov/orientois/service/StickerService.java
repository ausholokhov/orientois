package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.Sticker;

/**
 * Created by Anton on 12.02.2016.
 */
public interface StickerService extends CrudService<Sticker> {
}
