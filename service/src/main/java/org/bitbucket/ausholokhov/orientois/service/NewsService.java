package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.News;

/**
 * Created by Anton on 27.01.2016.
 */
public interface NewsService extends CrudService<News> {
}
