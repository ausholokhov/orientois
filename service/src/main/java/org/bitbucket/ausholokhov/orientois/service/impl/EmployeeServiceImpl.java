package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.EmployeeDao;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Iterator;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeDao dao;

    public EmployeeServiceImpl() {
    }

    @Autowired
    public EmployeeServiceImpl(EmployeeDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public Employee add(Employee e) {
        return dao.add(e);
    }

    @Override
    public Employee get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(Employee e) {
        dao.update(e);
    }

    @Override
    public List<Employee> getAll() {
        return dao.getAll();
    }

    @Override
    public List<Employee> criteriaFilter(List<Employee> employees, String criteria) {
        String[] criteriaSplit = criteria.split(" ");
        for (String criteriaPart : criteriaSplit) {
            containsMatcher(employees, criteriaPart);
        }
        return employees;
    }

    private void containsMatcher(List<Employee> employees, String criteriaPart) {
        Iterator<Employee> iterator = employees.iterator();
        while (iterator.hasNext()) {
            if (!iterator.next().toString().toLowerCase().contains(criteriaPart.toLowerCase())) {
                iterator.remove();
            }
        }
    }

    @Override
    public List<Employee> getByOffice(Long officeId) {
        return dao.getByOffice(officeId);
    }

    @Override
    public List<Employee> getEmployeesByCriteria(String criteria) {
        return dao.getEmployeesByCriteria(criteria);
    }

    @Override
    public Employee getEmployeeByAccount(Account a) {
        return dao.getEmployeeByAccount(a);
    }
}
