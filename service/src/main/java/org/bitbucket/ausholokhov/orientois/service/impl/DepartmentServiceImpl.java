package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.DepartmentDao;
import org.bitbucket.ausholokhov.orientois.models.Department;
import org.bitbucket.ausholokhov.orientois.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    private DepartmentDao dao;

    public DepartmentServiceImpl() {
    }

    @Autowired
    public DepartmentServiceImpl(DepartmentDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public Department add(Department d) {
        return dao.add(d);
    }

    @Override
    public Department get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(Department d) {
        dao.update(d);
    }

    @Override
    public List<Department> getAll() {
        return dao.getAll();
    }
}
