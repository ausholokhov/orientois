package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.StickerDao;
import org.bitbucket.ausholokhov.orientois.models.Sticker;
import org.bitbucket.ausholokhov.orientois.service.StickerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 12.02.2016.
 */
@Service
public class StickerServiceImpl implements StickerService {


    private StickerDao dao;

    public StickerServiceImpl() {
    }

    @Autowired
    public StickerServiceImpl(StickerDao dao) {
        this.dao = dao;
    }


    @Override
    @Transactional
    public Sticker add(Sticker object) {
        return dao.add(object);
    }

    @Override
    public Sticker get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(Sticker object) {
        dao.update(object);
    }

    @Override
    public List<Sticker> getAll() {
        return dao.getAll();
    }
}
