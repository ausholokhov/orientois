package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.NewsDao;
import org.bitbucket.ausholokhov.orientois.models.News;
import org.bitbucket.ausholokhov.orientois.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 27.01.2016.
 */
@Service
public class NewsServiceImpl implements NewsService {

    private NewsDao dao;

    public NewsServiceImpl() {
    }

    @Autowired
    public NewsServiceImpl(NewsDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public News add(News news) {
        return dao.add(news);
    }

    @Override
    public News get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(News news) {
        dao.update(news);
    }

    @Override
    public List<News> getAll() {
        return dao.getAll();
    }
}
