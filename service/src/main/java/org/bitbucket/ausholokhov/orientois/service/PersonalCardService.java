package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.PersonalCard;

/**
 * Created by Anton on 21.11.2015.
 */
public interface PersonalCardService extends CrudService<PersonalCard> {
}
