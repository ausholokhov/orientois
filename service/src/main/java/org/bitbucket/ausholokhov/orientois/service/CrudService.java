package org.bitbucket.ausholokhov.orientois.service;

import java.util.List;

public interface CrudService<T>{
    T add(T object);

    T get(Long id);

    void delete(Long id);

    void update(T object);

    List<T> getAll();
}
