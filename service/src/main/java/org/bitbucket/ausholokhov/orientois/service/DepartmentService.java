package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.Department;

/**
 * Created by Anton on 21.11.2015.
 */
public interface DepartmentService extends CrudService<Department> {
}
