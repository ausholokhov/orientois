package org.bitbucket.ausholokhov.orientois.service.impl;

import org.bitbucket.ausholokhov.orientois.dao.AccountDao;
import org.bitbucket.ausholokhov.orientois.dao.EmployeeDao;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 30.11.2015.
 */
@Service
public class AccountServiceImpl implements AccountService {
    private AccountDao dao;

    public AccountServiceImpl() {
    }

    @Autowired
    public AccountServiceImpl(AccountDao dao) {
        this.dao = dao;
    }

    @Override
    @Transactional
    public Account add(Account a) {
        return dao.add(a);
    }

    @Override
    public Account get(Long id) {
        return dao.get(id);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        dao.delete(id);
    }

    @Override
    @Transactional
    public void update(Account a) {
        dao.update(a);
    }

    @Override
    public List<Account> getAll() {
        return dao.getAll();
    }

    @Override
    public boolean isValidAccount(Account a) {
        Account fromBase = dao.getByLogin(a.getLogin());
        return a.equals(fromBase);
    }

    @Override
    public Account getByLogin(String login) {
        return dao.getByLogin(login);
    }


}
