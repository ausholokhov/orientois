package org.bitbucket.ausholokhov.orientois.service;

import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;

/**
 * Created by Anton on 30.11.2015.
 */
public interface AccountService extends CrudService<Account> {
    boolean isValidAccount(Account a);

    Account getByLogin(String login);
}
