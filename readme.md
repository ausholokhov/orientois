## Information Organization System ##

**Functionality:**

* encrypting cookie data in 3des
* encrypting account password in md5
* search bar with autocompletion
* loading of employees, offices, stickers, news, resumes
* creation of employees using the registration form
* creation of draggable stickers (remember coordinates) on employees cards
* creation of news using the admin page
* edit employee, card, offices, resume
* delete employee, news, offices
* upload and change avatar
* download the resume in pdf format
* admin page for editing employees and offices

**Tools:**
JDK 7, Spring 4 Core/ MVC, JPA / Hibernate, jasypt, jQuery, AJAX, Sweet Notes, WebJars, Bootstrap, Jackson, JSP, JSTL, iTextPdf, MySQL, Apache Maven, Apache Tomcat, JUnit, Mockito.

**Notes:**

![dbscheme.PNG](https://bitbucket.org/repo/a67zjz/images/3348437942-dbscheme.PNG)

**Screenshots:**

![login.PNG](https://bitbucket.org/repo/a67zjz/images/2660475940-login.PNG)
![news.jpg](https://bitbucket.org/repo/a67zjz/images/3941445135-news.jpg)
![search.png](https://bitbucket.org/repo/a67zjz/images/1175396781-search.png)
![infoPage.PNG](https://bitbucket.org/repo/a67zjz/images/1049565372-infoPage.PNG)
![editPage.PNG](https://bitbucket.org/repo/a67zjz/images/4065846409-editPage.PNG)
![card.PNG](https://bitbucket.org/repo/a67zjz/images/3541370093-card.PNG)
![resumeform.PNG](https://bitbucket.org/repo/a67zjz/images/804584628-resumeform.PNG)
![resume.PNG](https://bitbucket.org/repo/a67zjz/images/3817607179-resume.PNG)
![adminEdit.PNG](https://bitbucket.org/repo/a67zjz/images/447139404-adminEdit.PNG)

**Sholokhov Anton** Final qualifying work, Nov 2015 - Jan 2016

Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)