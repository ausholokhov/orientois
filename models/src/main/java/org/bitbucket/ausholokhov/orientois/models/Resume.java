package org.bitbucket.ausholokhov.orientois.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Anton on 20.11.2015.
 */
@Entity()
@Table(name = "resume")
@NamedQuery(name = "Resume.getAll", query = "SELECT r FROM Resume r")
public class Resume extends BaseModel {
    private static final long serialVersionUID = -4977639156821537129L;

    @Column(name = "full_name")
    private String fullname;
    @Lob
    @Column(name = "main_education")
    private String mainEducation;
    @Lob
    @Column(name = "additional_education")
    private String additionalEducation;
    @Column(name = "prior_workplace")
    @Lob
    private String priorWorkplace;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "birthdate")
    private Date birthdate;
    @Lob
    @Column(name = "skills")
    private String skills;
    @Lob
    @Column(name = "professional_expirience")
    private String professionalExpirience;
    @Column(name = "ready_for_relocation")
    private boolean readyForRelocation;
    @Column(name = "has_children")
    private boolean hasChildren;
    @Lob
    @Column(name = "contact_info")
    private String contactInfo;
    @Lob
    @Column(name = "additional_data")
    private String additionalData;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMainEducation() {
        return mainEducation;
    }

    public void setMainEducation(String mainEducation) {
        this.mainEducation = mainEducation;
    }

    public String getAdditionalEducation() {
        return additionalEducation;
    }

    public void setAdditionalEducation(String additionalEducation) {
        this.additionalEducation = additionalEducation;
    }

    public String getPriorWorkplace() {
        return priorWorkplace;
    }

    public void setPriorWorkplace(String priorWorkplace) {
        this.priorWorkplace = priorWorkplace;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getProfessionalExpirience() {
        return professionalExpirience;
    }

    public void setProfessionalExpirience(String professionalExpirience) {
        this.professionalExpirience = professionalExpirience;
    }

    public boolean isReadyForRelocation() {
        return readyForRelocation;
    }

    public void setReadyForRelocation(boolean readyForRelocation) {
        this.readyForRelocation = readyForRelocation;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(String contactInfo) {
        this.contactInfo = contactInfo;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }
}
