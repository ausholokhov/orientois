package org.bitbucket.ausholokhov.orientois.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Anton on 20.11.2015.
 */
@Entity
@Table(name = "department")
@NamedQuery(name = "Department.getAll", query = "SELECT d FROM Department d ORDER BY name")
public class Department extends BaseModel {

    private static final long serialVersionUID = 8940671747245725980L;

    @Column(name = "department_name", nullable = false)
    private String name;
    @OneToOne(targetEntity = Employee.class, orphanRemoval = true)
    private Employee director;
    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Office> offices;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getDirector() {
        return director;
    }

    public void setDirector(Employee director) {
        this.director = director;
    }

    public List<Office> getOffices() {
        return offices;
    }

    public void setOffices(List<Office> offices) {
        this.offices = offices;
    }

    @Override
    public String toString() {
        return name;
    }

    @PreRemove
    private void preRemove() {
        for (Office office : offices) {
            office.setDepartment(null);
        }
        setOffices(null);
    }
}
