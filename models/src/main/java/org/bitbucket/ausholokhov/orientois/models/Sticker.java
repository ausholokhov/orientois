package org.bitbucket.ausholokhov.orientois.models;

import javax.persistence.*;

/**
 * Created by Anton on 07.02.2016.
 */
@Table
@Entity(name = "sticker")
@NamedQuery(name = "Sticker.getAll", query = "SELECT s FROM sticker s")
public class Sticker extends BaseModel{

    @Column(name = "message")
    private String message;
    @Column(name = "author")
    private String author;
    @Column
    private String position;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="PersonalCard_id")
    private PersonalCard personalCard;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public PersonalCard getPersonalCard() {
        return personalCard;
    }

    public void setPersonalCard(PersonalCard personalCard) {
        this.personalCard = personalCard;
    }
}
