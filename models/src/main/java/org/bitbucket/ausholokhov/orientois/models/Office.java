package org.bitbucket.ausholokhov.orientois.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Anton on 20.11.2015.
 */
@Entity()
@Table(name = "office")
@NamedQuery(name = "Office.getAll", query = "SELECT o from Office o")
public class Office extends BaseModel {
    private static final long serialVersionUID = 7008210887244019154L;

    public Office() {
    }

    public Office(String name) {
        this.name = name;
    }

    @Column(name = "office_name", nullable = false)
    private String name;
    @OneToOne(targetEntity = Employee.class)
    private Employee chief;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;
    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="office")
    private List<Employee> employees;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getChief() {
        return chief;
    }

    public void setChief(Employee chief) {
        this.chief = chief;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Office{" +
                "name='" + name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Office office = (Office) o;
        return !(name != null ? !name.equals(office.name) : office.name != null) &&
                !(department != null ? !department.equals(office.department) : office.department != null);
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (department != null ? department.hashCode() : 0);
        return result;
    }
}
