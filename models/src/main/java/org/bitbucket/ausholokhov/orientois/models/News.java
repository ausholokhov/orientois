package org.bitbucket.ausholokhov.orientois.models;

import javax.persistence.*;
import java.util.Base64;

/**
 * Created by Anton on 27.01.2016.
 */
@Entity
@Table(name = "news")
@NamedQuery(name = "News.getAll", query = "SELECT n FROM News n")
public class News extends BaseModel{

    @Column(name = "header")
    private String header;
    @Column(name = "body")
    @Lob
    private String body;
    @Lob
    @Column(name = "image", length = 5145728)
    @Basic(fetch = FetchType.LAZY, optional = false)
    private byte[] image;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getImage() {
        return new String(Base64.getEncoder().encode(image));
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

}
