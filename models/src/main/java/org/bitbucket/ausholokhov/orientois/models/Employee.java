package org.bitbucket.ausholokhov.orientois.models;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Anton on 20.11.2015.
 */
@Entity
@Table(name = "employee")
@NamedQuery(name = "Employee.getAll", query = "SELECT e FROM Employee e")
public class Employee extends BaseModel {
    private static final long serialVersionUID = 4780857404424776657L;

    @Column(name = "first_name", length = 20, nullable = false)
    private String firstname;
    @Column(name = "middle_name", length = 20, nullable = false)
    private String middlename;
    @Column(name = "last_name", length = 20, nullable = false)
    private String lastname;
    @Column(name = "email", length = 30, nullable = false)
    private String email;
    @ElementCollection
    private List<String> contactPhones;
    @Column(name = "address", length = 100)
    private String address;
    @ManyToOne
    @JoinColumn(name = "office_id")
    private Office office;
    @OneToOne(targetEntity = Resume.class, orphanRemoval = true, cascade = CascadeType.ALL)
    private Resume resume;
    @OneToOne(targetEntity = Account.class, orphanRemoval = true, cascade = CascadeType.ALL)
    private Account account;
    @OneToOne(targetEntity = PersonalCard.class, orphanRemoval = true, cascade = CascadeType.ALL)
    private PersonalCard personalCard;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getContactPhones() {
        return contactPhones;
    }

    public void setContactPhones(List<String> contactPhones) {
        this.contactPhones = contactPhones;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Office getOffice() {
        return office;
    }

    public void setOffice(Office office) {
        this.office = office;
    }

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    public void setPersonalCard(PersonalCard personalCard) {
        this.personalCard = personalCard;
    }

    public PersonalCard getPersonalCard() {
        return personalCard;
    }

    @Override
    public String toString() {
        return firstname + " " + middlename + " " + lastname + " " + email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return firstname != null ? firstname.equals(employee.firstname) : employee.firstname == null
                && (middlename != null ? middlename.equals(employee.middlename) : employee.middlename == null
                && !(lastname != null ? !lastname.equals(employee.lastname) : employee.lastname != null)
                && !(email != null ? !email.equals(employee.email) : employee.email != null));
    }

    @Override
    public int hashCode() {
        int result = firstname != null ? firstname.hashCode() : 0;
        result = 31 * result + (middlename != null ? middlename.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        return result;
    }


}
