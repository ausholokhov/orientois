package org.bitbucket.ausholokhov.orientois.models;

import org.apache.commons.io.IOUtils;

import javax.persistence.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Entity
@Table(name = "personal_card")
@NamedQuery(name = "PersonalCard.getAll", query = "SELECT p from PersonalCard p")
public class PersonalCard extends BaseModel {
    private static final long serialVersionUID = -2536677155333847467L;

    @Lob
    @Column(name = "photo", length = 3145728)
    private byte[] photo;
    @Column(name = "post")
    private String post;
    @OneToMany(mappedBy="personalCard", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Sticker> stickers;

    public String getPhoto() {
        if (photo == null || photo.length == 0) {
            InputStream in = getClass().getResourceAsStream("/no-avatar.png");
            try {
                photo = IOUtils.toByteArray(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new String(Base64.getEncoder().encode(photo));
    }

    public byte[] getPhotoBytes() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public List<Sticker> getStickers() {
        return stickers;
    }

    public void setStickers(List<Sticker> stickers) {
        this.stickers = stickers;
    }
}
