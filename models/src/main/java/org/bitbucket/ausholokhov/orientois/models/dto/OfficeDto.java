package org.bitbucket.ausholokhov.orientois.models.dto;

import java.io.Serializable;

/**
 * Created by Anton on 28.01.2016.
 */
public class OfficeDto implements Serializable {
    public Long id;
    public String name;
}
