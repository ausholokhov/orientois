package org.bitbucket.ausholokhov.orientois.models.dto;

import java.io.Serializable;

/**
 * Created by Anton on 05.12.2015.
 */
public class EmployeeDto implements Serializable {

    private static final long serialVersionUID = 6815060496642145316L;

    public Long id;

    public String firstname;

    public String middlename;

    public String lastname;

    public String email;
}
