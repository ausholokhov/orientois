package org.bitbucket.ausholokhov.orientois.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Anton on 20.12.2015.
 */
@MappedSuperclass
public abstract class BaseModel implements Serializable {
    private static final long serialVersionUID = 5272270410058015846L;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isNew() {
        return id == null;
    }
}
