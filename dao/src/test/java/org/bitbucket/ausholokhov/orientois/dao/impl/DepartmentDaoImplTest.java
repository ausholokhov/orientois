package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.DepartmentDao;
import org.bitbucket.ausholokhov.orientois.dao.EmployeeDao;
import org.bitbucket.ausholokhov.orientois.models.Department;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.bitbucket.ausholokhov.orientois.models.Office;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.PreRemove;
import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by Anton on 28.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml"})
@Transactional
public class DepartmentDaoImplTest {

    private static Connection connection;

    @Autowired
    private DepartmentDao dao;

    @BeforeClass
    public static void onBeforeClass() throws Exception {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testois", "root", "1234");
    }

    @Before
    public void createDB() throws Exception {
        Scanner scanner = new Scanner(new FileInputStream("src/test/resources/META-INF/createTestDB.sql"), "UTF8");
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement statement = connection.createStatement();
        while (scanner.hasNext()) {
            String line = scanner.next();
            if (line.trim().length() > 0) {
                statement.execute(line);
            }
        }
        scanner.close();
    }

    @Test
    public void testGetAll() throws Exception {
        List<Department> departments = dao.getAll();
        final int expectedEmployeesCount = 2;
        Assert.assertEquals(departments.size(), expectedEmployeesCount);
    }

    @Test

    public void testAdd() throws Exception {
        Department department = new Department();
        department.setName("TestDep");
        dao.add(department);
        Assert.assertEquals(dao.getAll().size(), 3);
    }

    @Test
    public void testGet() throws Exception {
        Department department = dao.get(1L);
        Assert.assertEquals(department.getName(), "Департамент информационных технологий");
    }

    @Test
    public void testDelete() throws Exception {
        dao.delete(1L);
        Assert.assertEquals(dao.get(1L), null);
    }

    @Test
    public void testUpdate() throws Exception {
        Department department = dao.get(1L);
        String testData = "DIT";
        department.setName(testData);
        dao.update(department);
        department = dao.get(1L);
        Assert.assertEquals(department.getName(), testData);
    }
}