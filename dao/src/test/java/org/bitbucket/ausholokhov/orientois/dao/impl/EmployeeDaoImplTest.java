package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.EmployeeDao;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml"})
@Transactional
public class EmployeeDaoImplTest {

    @Autowired
    private EmployeeDao dao;

    @BeforeClass
    public static void createDB() throws Exception {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testois", "root", "1234");
        Scanner scanner = new Scanner(new FileInputStream("src/test/resources/META-INF/createTestDB.sql"), "UTF8");
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement statement = connection.createStatement();
        while (scanner.hasNext()) {
            String line = scanner.next();
            if (line.trim().length() > 0) {
                statement.execute(line);
            }
        }
        statement.close();
        scanner.close();
        connection.close();
    }

    @Test
    public void testGetAll() throws Exception {
        List<Employee> employees = dao.getAll();
        final int expectedEmployeesCount = 4;
        Assert.assertEquals(employees.size(), expectedEmployeesCount);
    }

    @Test
    public void testAdd() throws Exception {
        Employee employee = new Employee();
        employee.setFirstname("Ivan");
        employee.setMiddlename("Ivanovich");
        employee.setLastname("Ivanov");
        employee.setEmail("vano@gmail.com");
        employee.setAddress("adress");
        dao.add(employee);
        Assert.assertEquals(dao.getAll().size(), 5);
    }

    @Test
    public void testGet() throws Exception {
        Employee employee = dao.get(1L);
        Assert.assertEquals(employee.getLastname(), "Шолохов");
    }

    @Test
    public void testDelete() throws Exception {
        dao.delete(1L);
        Assert.assertEquals(dao.get(1L), null);
    }

    @Test
    public void testUpdate() throws Exception {
        Employee employee = dao.get(1L);
        String testEmail = "test@mail.ru";
        employee.setAddress(testEmail);
        dao.update(employee);
        employee = dao.get(1L);
        Assert.assertEquals(employee.getAddress(), testEmail);
    }

    @Test
    public void testGetEmployeesByCriteria() throws Exception {
        Employee employee = dao.getEmployeesByCriteria("антон").get(0);
        Assert.assertEquals(employee.getFirstname(), "Антон");
    }
}