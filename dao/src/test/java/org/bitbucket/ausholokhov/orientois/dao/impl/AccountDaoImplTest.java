package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.AccountDao;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Anton on 30.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml"})
@Transactional
public class AccountDaoImplTest {

    private static Connection connection;

    @Autowired
    private AccountDao dao;

    @BeforeClass
    public static void onBeforeClass() throws Exception {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testois", "root", "1234");
    }

    @Before
    public void createDB() throws Exception {
        Scanner scanner = new Scanner(new FileInputStream("src/test/resources/META-INF/createTestDB.sql"), "UTF8");
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement statement = connection.createStatement();
        while (scanner.hasNext()) {
            String line = scanner.next();
            if (line.trim().length() > 0) {
                statement.execute(line);
            }
        }
        scanner.close();
    }

    @Test
    @Transactional
    public void testGetByLogin() throws Exception {
        String login = "trus";
        Account account = dao.getByLogin(login);
        Assert.assertEquals(login, account.getLogin());
    }

    @Test
    public void testAdd() throws Exception {
        Account expected = new Account("login", "password");
        dao.add(expected);
        Account resultGet = dao.getByLogin(expected.getLogin());
        Assert.assertEquals(expected.getLogin(), resultGet.getLogin());
    }

    @Test
    public void testGet() throws Exception {
        Account resultGet = dao.get(1L);
        Assert.assertEquals(resultGet.getLogin(), "admin");
    }

    @Test
    public void testDelete() throws Exception {
        dao.delete(1L);
        Assert.assertEquals(dao.get(1L), null);
    }

    @Test
    public void testUpdate() throws Exception {
        Account account = dao.get(1L);
        String newPass = "1234";
        account.setPassword(newPass);
        dao.update(account);
        Account updatedAcc = dao.get(1L);
        Assert.assertEquals(updatedAcc.getPassword(), newPass);
    }

    @Test
    public void testGetAll() throws Exception {
        int numberOfAccounts = 4;
        List<Account> accounts = dao.getAll();
        Assert.assertEquals(numberOfAccounts, accounts.size());
    }
}