package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.PersonalCardDao;
import org.bitbucket.ausholokhov.orientois.dao.ResumeDao;
import org.bitbucket.ausholokhov.orientois.models.PersonalCard;
import org.bitbucket.ausholokhov.orientois.models.Resume;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Date;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by Anton on 28.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml"})
@Transactional
public class PersonalCardDaoImplTest {

    private static Connection connection;

    @Autowired
    private PersonalCardDao dao;

    @BeforeClass
    public static void onBeforeClass() throws Exception {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testois", "root", "1234");
    }

    @Before
    public void createDB() throws Exception {
        Scanner scanner = new Scanner(new FileInputStream("src/test/resources/META-INF/createTestDB.sql"), "UTF8");
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement statement = connection.createStatement();
        while (scanner.hasNext()) {
            String line = scanner.next();
            if (line.trim().length() > 0) {
                statement.execute(line);
            }
        }
        scanner.close();
    }

    @Test
    public void testGetAll() throws Exception {
        int expectedSize = 4;
        int resultSize = dao.getAll().size();
        Assert.assertEquals(expectedSize, resultSize);
    }

    @Test
    public void testAdd() throws Exception {
        PersonalCard personalCard = new PersonalCard();
        personalCard.setPost("testpost");
        dao.add(personalCard);
        Assert.assertEquals(dao.getAll().size(), 5);
    }

    @Test
    public void testGet() throws Exception {
        PersonalCard personalCard = dao.get(1L);
        Assert.assertNotNull(personalCard);
    }
}