package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.OfficeDao;
import org.bitbucket.ausholokhov.orientois.models.Office;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Scanner;

import static org.junit.Assert.*;

/**
 * Created by Anton on 28.11.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml"})
@Transactional
public class OfficeDaoImplTest {

    private static Connection connection;

    @Autowired
    private OfficeDao dao;

    @BeforeClass
    public static void onBeforeClass() throws Exception {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/testois", "root", "1234");
    }

    @Before
    public void createDB() throws Exception {
        Scanner scanner = new Scanner(new FileInputStream("src/test/resources/META-INF/createTestDB.sql"), "UTF8");
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement statement = connection.createStatement();
        while (scanner.hasNext()) {
            String line = scanner.next();
            if (line.trim().length() > 0) {
                statement.execute(line);
            }
        }
        scanner.close();
    }

    @Test
    public void testGetAll() throws Exception {
        int expectedSize = 5;
        int resultSize = dao.getAll().size();
        Assert.assertEquals(expectedSize, resultSize);
    }

    @Test
    public void testAdd() throws Exception {
        Office office = dao.get(1L);
        String testName = "testOffice";
        office.setName(testName);
        dao.add(office);
        Assert.assertEquals(dao.get(1L).getName(), testName);
    }

    @Test
    public void testGet() throws Exception {
        Office office = dao.get(1L);
        Assert.assertEquals(office.getName(), "Системные администраторы");
}

    @Test
    public void testUpdate() throws Exception {
        Office office = dao.get(1L);
        String testData = "Сисадмины";
        office.setName(testData);
        dao.update(office);
        office = dao.get(1L);
        Assert.assertEquals(office.getName(), testData);
    }
}