package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.AccountDao;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 30.11.2015.
 */
@Repository
public class AccountDaoImpl implements AccountDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Account add(Account a) {
        return em.merge(a);
    }

    @Override
    public Account get(Long id) {
        return em.find(Account.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(Account a) {
        em.merge(a);
    }

    @Override
    public List<Account> getAll() {
        return em.createNamedQuery("Account.getAll", Account.class).getResultList();
    }

    @Override
    public Account getByLogin(String login) {
        TypedQuery<Account> query = em.createQuery("SELECT a FROM Account a WHERE a.login = ?1", Account.class);
        return query.setParameter(1, login).getSingleResult();
    }

}
