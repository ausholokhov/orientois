package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.News;

/**
 * Created by Anton on 27.01.2016.
 */
public interface NewsDao extends CrudDao<News> {
}
