package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.OfficeDao;
import org.bitbucket.ausholokhov.orientois.models.Office;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Repository
public class OfficeDaoImpl implements OfficeDao {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Office add(Office o) {
        return em.merge(o);
    }

    @Override
    public Office get(Long id) {
        return em.find(Office.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(Office o) {
        em.merge(o);
    }

    @Override
    public List<Office> getAll() {
        return em.createNamedQuery("Office.getAll", Office.class).getResultList();
    }
}
