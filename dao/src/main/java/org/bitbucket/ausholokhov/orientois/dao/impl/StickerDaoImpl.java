package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.StickerDao;
import org.bitbucket.ausholokhov.orientois.models.Sticker;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created by Anton on 12.02.2016.
 */
@Repository
public class StickerDaoImpl implements StickerDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Sticker add(Sticker object) {
        return em.merge(object);
    }

    @Override
    public Sticker get(Long id) {
        return em.find(Sticker.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(Sticker object) {
        em.merge(object);
    }

    @Override
    public List<Sticker> getAll() {
        return em.createNamedQuery("Sticker.getAll", Sticker.class).getResultList();
    }
}
