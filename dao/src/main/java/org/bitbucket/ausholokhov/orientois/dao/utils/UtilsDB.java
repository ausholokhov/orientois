package org.bitbucket.ausholokhov.orientois.dao.utils;

import org.bitbucket.ausholokhov.orientois.models.*;
import org.jasypt.util.password.StrongPasswordEncryptor;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.List;

public class UtilsDB {
    private static EntityManagerFactory factory = Persistence.createEntityManagerFactory("OIS");

    public static void main(String[] args) {
//        create();
//        update();
//        employees();
    }

    public static void create() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.close();
        factory.close();
        System.out.println("----CREATED----");
    }

    public static void update() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();

        //Департамент инф. технологий
        List<Office> ditOffices = new ArrayList<>();
        Office sysadminsOffice = new Office("Системные администраторы");
        ditOffices.add(sysadminsOffice);
        Office communicationsOffice = new Office("Отдел телекоммуникаций и связи");
        ditOffices.add(communicationsOffice);
        Department dit = new Department();
        dit.setName("Департамент информационных технологий");
        dit.setOffices(ditOffices);
        for (Office ditOffice : ditOffices) {
            ditOffice.setDepartment(dit);
        }
        em.merge(dit);

        //Департамент по работе с персоналом
        List<Office> accOffices = new ArrayList<>();
        Office buhOffice = new Office("Бухгалтерия");
        accOffices.add(buhOffice);
        Office personalOffice = new Office("Отдел избиения персонала");
        accOffices.add(personalOffice);
        Department acc = new Department();
        acc.setName("Департамент по работе с персоналом");
        acc.setOffices(accOffices);
        for (Office accOffice : accOffices) {
            accOffice.setDepartment(acc);
        }
        em.merge(acc);

        //Сотрудники
        StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();
        Account adminAcc = new Account("admin", encryptor.encryptPassword("Admin1234"));
        Account nikulinAcc = new Account("balbes", encryptor.encryptPassword("Balbes1234"));
        Account vicinAcc = new Account("trus", encryptor.encryptPassword("Trus1234"));
        Account bivaliyAcc = new Account("bivaliy", encryptor.encryptPassword("Bivaliy1234"));

        Employee admin = new Employee();
        admin.setAccount(adminAcc);
        admin.setFirstname("Антон");
        admin.setMiddlename("Юрьевич");
        admin.setLastname("Шолохов");
        admin.setEmail("admin@gmail.com");
        admin.setResume(new Resume());
        admin.setContactPhones(getPhones());

        Employee balbes = new Employee();
        balbes.setAccount(nikulinAcc);
        balbes.setFirstname("Юрий");
        balbes.setMiddlename("Владимирович");
        balbes.setLastname("Никулин");
        balbes.setEmail("balbes@gmail.com");
        balbes.setResume(new Resume());
        balbes.setContactPhones(getPhones());

        Employee trus = new Employee();
        trus.setAccount(vicinAcc);
        trus.setFirstname("Георгий");
        trus.setMiddlename("Михайлович");
        trus.setLastname("Вицин");
        trus.setEmail("trus@gmail.com");
        trus.setResume(new Resume());
        trus.setContactPhones(getPhones());

        Employee bivaliy = new Employee();
        bivaliy.setAccount(bivaliyAcc);
        bivaliy.setFirstname("Евгений");
        bivaliy.setMiddlename("Александрович");
        bivaliy.setLastname("Моргунов");
        bivaliy.setEmail("bivaliy@gmail.com");
        bivaliy.setResume(new Resume());
        bivaliy.setContactPhones(getPhones());

        em.merge(admin);
        em.merge(trus);
        em.merge(balbes);
        em.merge(bivaliy);

        em.getTransaction().commit();
        factory.close();
        System.out.println("----UPDATED----");
    }

    private static List<String> getPhones() {
        int maxPhones = 5;
        int phoneLength = 10000000;
        int randomPhonesCount = (int) (Math.random() * maxPhones);
        List<String> phones = new ArrayList<>();
        for (int i = 0; i < randomPhonesCount; i++) {
            phones.add(String.valueOf((int) (Math.random() * phoneLength)));
        }
        return phones;
    }


    public static void employees() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        for (int i = 1; i < 5000; i++) {
            Employee employee = new Employee();
            employee.setAccount(new Account("123"+i,i+""));
            employee.setFirstname("Евгений" +i);
            employee.setMiddlename("Александрович"+i);
            employee.setLastname("Моргунов"+i);
            employee.setEmail("bivaliy@gmail.com"+i);
            employee.setResume(new Resume());
            employee.setContactPhones(getPhones());
            em.merge(employee);
            em.flush();
//           em.remove(em.find(Employee.class, (long)i));
        }
        em.getTransaction().commit();
    }
}
