package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;

import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
public interface EmployeeDao extends CrudDao<Employee> {
    List<Employee> getByOffice(Long officeId);

    List<Employee> getEmployeesByCriteria(String criteria);

    Employee getEmployeeByAccount(Account a);
}
