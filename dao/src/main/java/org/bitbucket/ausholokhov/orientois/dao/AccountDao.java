package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.Account;

/**
 * Created by Anton on 30.11.2015.
 */
public interface AccountDao extends CrudDao<Account> {
    Account getByLogin(String login);
}
