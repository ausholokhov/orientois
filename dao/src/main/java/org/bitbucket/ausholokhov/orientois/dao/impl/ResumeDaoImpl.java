package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.ResumeDao;
import org.bitbucket.ausholokhov.orientois.models.Resume;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PreRemove;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Service
public class ResumeDaoImpl implements ResumeDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Resume add(Resume r) {
        return em.merge(r);
    }

    @Override
    public Resume get(Long id) {
        return em.find(Resume.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(Resume r) {
        em.merge(r);
    }

    @Override
    public List<Resume> getAll() {
        return em.createNamedQuery("Resume.getAll", Resume.class).getResultList();
    }


}
