package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.Sticker;

/**
 * Created by Anton on 12.02.2016.
 */
public interface StickerDao extends CrudDao<Sticker> {
}
