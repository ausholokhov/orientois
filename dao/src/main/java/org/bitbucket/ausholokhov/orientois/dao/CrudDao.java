package org.bitbucket.ausholokhov.orientois.dao;

import java.util.List;

public interface CrudDao<T> {

    T add(T object);

    T get(Long id);

    void delete(Long id);

    void update(T object);

    List<T> getAll();
}
