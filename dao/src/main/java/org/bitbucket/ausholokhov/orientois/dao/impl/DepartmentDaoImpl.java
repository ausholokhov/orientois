package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.DepartmentDao;
import org.bitbucket.ausholokhov.orientois.models.Department;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Repository
public class DepartmentDaoImpl implements DepartmentDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Department add(Department d) {
        return em.merge(d);
    }

    @Override
    public Department get(Long id) {
        return em.find(Department.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(Department d) {
        em.merge(d);
    }

    @Override
    public List<Department> getAll() {
        return em.createNamedQuery("Department.getAll", Department.class).getResultList();
    }
}
