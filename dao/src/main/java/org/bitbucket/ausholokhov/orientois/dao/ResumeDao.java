package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.Resume;

/**
 * Created by Anton on 21.11.2015.
 */
public interface ResumeDao extends CrudDao<Resume> {
}
