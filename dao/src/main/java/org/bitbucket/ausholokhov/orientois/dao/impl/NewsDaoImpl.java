package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.NewsDao;
import org.bitbucket.ausholokhov.orientois.models.News;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 27.01.2016.
 */
@Repository
public class NewsDaoImpl implements NewsDao {
    @PersistenceContext
    private EntityManager em;

    @Override
    public News add(News news) {
        return em.merge(news);
    }

    @Override
    public News get(Long id) {
        return em.find(News.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(News news) {
        em.merge(news);
    }

    @Override
    public List<News> getAll() {
        return em.createNamedQuery("News.getAll", News.class).getResultList();
    }
}
