package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.PersonalCardDao;
import org.bitbucket.ausholokhov.orientois.models.PersonalCard;
import org.bitbucket.ausholokhov.orientois.models.Sticker;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Anton on 21.11.2015.
 */
@Repository
public class PersonalCardDaoImpl implements PersonalCardDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public PersonalCard add(PersonalCard p) {
        return em.merge(p);
    }

    @Override
    public PersonalCard get(Long id) {
        return em.find(PersonalCard.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(PersonalCard p) {
        em.merge(p);
    }

    @Override
    public List<PersonalCard> getAll() {
        return em.createNamedQuery("PersonalCard.getAll", PersonalCard.class).getResultList();
    }
}
