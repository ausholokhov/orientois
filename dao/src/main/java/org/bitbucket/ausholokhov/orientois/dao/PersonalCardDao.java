package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.PersonalCard;

/**
 * Created by Anton on 21.11.2015.
 */
public interface PersonalCardDao extends CrudDao<PersonalCard> {
}
