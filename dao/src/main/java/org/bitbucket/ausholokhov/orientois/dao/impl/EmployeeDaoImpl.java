package org.bitbucket.ausholokhov.orientois.dao.impl;

import org.bitbucket.ausholokhov.orientois.dao.EmployeeDao;
import org.bitbucket.ausholokhov.orientois.models.Account;
import org.bitbucket.ausholokhov.orientois.models.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Employee add(Employee e) {
        return em.merge(e);
    }

    @Override
    public Employee get(Long id) {
        return em.find(Employee.class, id);
    }

    @Override
    public void delete(Long id) {
        em.remove(get(id));
    }

    @Override
    public void update(Employee e) {
        em.merge(e);
    }

    @Override
    public List<Employee> getAll() {
        return em.createNamedQuery("Employee.getAll", Employee.class).getResultList();
    }

    @Override
    public List<Employee> getByOffice(Long officeId) {
        TypedQuery<Employee> query = em.createQuery(
                "SELECT e FROM Employee e WHERE e.office.id = :officeId", Employee.class);
        return query.setParameter("officeId", officeId).getResultList();
    }

    @Override
    public List<Employee> getEmployeesByCriteria(String criteria) {
        List<List<Employee>> selectResults = new ArrayList<>();
        String[] criterias = criteria.trim().split(" ");
        for (String s : criterias) {
            TypedQuery<Employee> query = em.createQuery(
                    "SELECT e FROM Employee e WHERE UPPER(e.firstname) LIKE UPPER(:s) " +
                            "OR UPPER(e.middlename) LIKE UPPER(:s) " +
                            "OR UPPER(e.lastname) LIKE UPPER(:s) ", Employee.class);
            selectResults.add(query.setParameter("s", "%" + s + "%").setMaxResults(10).getResultList());
        }
        List<Employee> result = new LinkedList<>(selectResults.get(0));
        for (int i = 1; i < selectResults.size(); i++) {
            result.retainAll(selectResults.get(i));
        }
        return result;
    }

    @Override
    public Employee getEmployeeByAccount(Account a) {
        TypedQuery<Employee> query = em.createQuery("SELECT e FROM Employee e WHERE e.account.login = ?1", Employee.class);
        return query.setParameter(1, a.getLogin()).getSingleResult();
    }
}
