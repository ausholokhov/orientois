package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.Department;

/**
 * Created by Anton on 21.11.2015.
 */
public interface DepartmentDao extends CrudDao<Department> {
}
