package org.bitbucket.ausholokhov.orientois.dao;

import org.bitbucket.ausholokhov.orientois.models.Office;

/**
 * Created by Anton on 21.11.2015.
 */
public interface OfficeDao extends CrudDao<Office> {
}
